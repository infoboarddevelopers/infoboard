package org.lestvitsa.InfoBoard.LayoutBuilder;

import java.util.ArrayDeque;
import java.util.Deque;

import org.lestvitsa.InfoBoard.InfoModel.Item;
import org.lestvitsa.InfoBoard.InfoModel.ItemActionHandler;
import org.lestvitsa.InfoBoard.InfoModel.ItemActionHandler.Action;
import org.lestvitsa.InfoBoard.InfoModel.Utils.CoordinateMapper;
import org.lestvitsa.InfoBoard.InfoModel.Utils.Coordinates;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.toDegrees;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.util.Duration;

public class CirclesWheel extends Group {
	public final int COUNT_OF_CIRCLES;
	private final int COUNT_OF_CIRCLES_ON_PANE;

	private ItemCircle[] itemCircleArray;
	private double[] itemCircleAngles;
	private Deque<Circle> spareCircles;

	private Rotate rotate;
	private Scale[] scaleTransforms;
	private InvalidationListener updateScaleOfCircles;

	/**
	 * positive clockwise direction 1 - positive, (-1) - negative
	 */
	private IntegerProperty directionOfRotation = new SimpleIntegerProperty(1);

	private double previousAngle;
	private double angle;

	private void setScaleAnimation(ReadOnlyDoubleProperty angleProperty) {
		updateScaleOfCircles = new InvalidationListener() {

			private double deltaAngle = 360 / COUNT_OF_CIRCLES_ON_PANE;

			@Override
			public void invalidated(Observable observable) {
				double wheelAngle = rotate.getAngle();

				for (int i = 0; i < itemCircleArray.length; i++) {
					double newCircleAngle = (itemCircleAngles[i] + wheelAngle - 90) % 360;
					if (newCircleAngle > 180) {
						newCircleAngle -= 360;
					}

					double scaleValue = f(newCircleAngle);

					scaleTransforms[i].setX(scaleValue);
					scaleTransforms[i].setY(scaleValue);

				}
			}

			private double f(double a) {
				double res = abs(a) > deltaAngle / 2 ? 1 : 1.5 - abs(a) / (deltaAngle / 2) * (5.0 / 10);
				return res;

				// for 6 circles
				// return abs(a) > 50 ? 1 : 1.5 - abs(a) / 100;
			}
		};

		angleProperty.addListener(updateScaleOfCircles);
	}

	private void setChangeCircleAnimation(ReadOnlyDoubleProperty angleProperty) {

		InvalidationListener changeCircleInFrame = new InvalidationListener() {

			private double[] prevCircleAngle = new double[itemCircleArray.length];
			double wheelAngle;
			boolean isItStartOfAnimation = true;

			private void rememberAngles() {
				wheelAngle = rotate.getAngle();
				for (int i = 0; i < prevCircleAngle.length; i++) {
					prevCircleAngle[i] = getNewAngleForCircle(i);
				}
			}

			private double getNewAngleForCircle(int index) {
				double newCircleAngle = (((itemCircleAngles[index] + wheelAngle + 90) % 360) + 360) % 360;
				if (newCircleAngle > 180) {
					newCircleAngle -= 360;
				}

				return newCircleAngle;

			}

			@Override
			public void invalidated(Observable observable) {
				wheelAngle = rotate.getAngle();

				for (int i = 0; i < itemCircleArray.length; i++) {
					if (isItStartOfAnimation) {
						rememberAngles();
						isItStartOfAnimation = false;
					}
					double newCircleAngle = getNewAngleForCircle(i);

					if (abs(newCircleAngle - prevCircleAngle[i]) < 90) {
						if (prevCircleAngle[i] > 0 && newCircleAngle <= 0) {
							spareCircles.addLast(itemCircleArray[i].getFrontCircle());
						}

						if (prevCircleAngle[i] < 0 && newCircleAngle >= 0) {
							spareCircles.addFirst(itemCircleArray[i].getFrontCircle());
						}

						if (prevCircleAngle[i] <= 0 && newCircleAngle > 0) {
							itemCircleArray[i].changeFrontCircle(spareCircles.removeLast());
						}

						if (prevCircleAngle[i] >= 0 && newCircleAngle < 0) {
							itemCircleArray[i].changeFrontCircle(spareCircles.removeFirst());
						}

					}
					prevCircleAngle[i] = newCircleAngle;
				}
			}
		};

		angleProperty.addListener(changeCircleInFrame);
	}

	public CirclesWheel(final int countOfCirclesOnPane, Item[] items, ItemActionHandler actionHandler)
			throws Exception {
		COUNT_OF_CIRCLES_ON_PANE = countOfCirclesOnPane;
		COUNT_OF_CIRCLES = items.length;
		if (COUNT_OF_CIRCLES < COUNT_OF_CIRCLES_ON_PANE)
			throw new Exception("The number of items to display must be at least " + COUNT_OF_CIRCLES_ON_PANE);

		itemCircleArray = new ItemCircle[COUNT_OF_CIRCLES_ON_PANE];
		itemCircleAngles = new double[COUNT_OF_CIRCLES_ON_PANE];
		spareCircles = new ArrayDeque<Circle>(COUNT_OF_CIRCLES - COUNT_OF_CIRCLES_ON_PANE + 2);

		for (int i = 0; i < COUNT_OF_CIRCLES; i++) {

			Circle c = new Circle();
			if (items[i] != null)
				c.setFill(new ImagePattern(items[i].getIconImage()));
			else
				c.setFill(new ImagePattern(Item.createIconWithText(null, Integer.toString(i + 1))));

			// TODO handler
			final Item itemForIndex = items[i];
			if (itemForIndex != null)
				c.setOnMouseReleased(new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent event) {
						if (!event.isSynthesized()) { // TUIO synthesizes duplicate mouse events
							System.out.println("Mouse released in item " + itemForIndex);

							actionHandler.handleItemAction(itemForIndex, Action.Open);
							event.consume();
						}
					}
				});

			if (i < COUNT_OF_CIRCLES_ON_PANE) {
				ItemCircle itemCircle = new ItemCircle(c);
				itemCircle.getStyleClass().add("pressableItem");
				this.getChildren().add(itemCircle);
				itemCircleArray[i] = itemCircle;
			} else {
				spareCircles.addLast(c);
			}
		}

		rotate = new Rotate(0);
		this.getTransforms().add(rotate);

		setChangeCircleAnimation(rotate.angleProperty());

		scaleTransforms = new Scale[itemCircleArray.length];
		for (int i = 0; i < scaleTransforms.length; i++) {
			scaleTransforms[i] = new Scale();
			itemCircleArray[i].getTransforms().add(scaleTransforms[i]);
		}

		setScaleAnimation(rotate.angleProperty());

		// to scale the first circle on startup
		updateScaleOfCircles.invalidated(null);
	}

	public void updatePositionAndSize(double centerX, double centerY, double wheelRadius, double itemCircleRadius) {

		CoordinateMapper mapper = new CoordinateMapper();
		mapper.setGridCenter(new Point2D(centerX, centerY));

		double angleOffset = 2 * PI / COUNT_OF_CIRCLES_ON_PANE;
		double coordinateAxisOffset = -PI / 2 + angleOffset / 2 * (COUNT_OF_CIRCLES_ON_PANE % 2)
				- /* accuracy error */PI / 180;

		for (int i = 0; i < COUNT_OF_CIRCLES_ON_PANE; i++) {
			double angrad = i * angleOffset + coordinateAxisOffset;

			itemCircleArray[i].setRadius(itemCircleRadius);
			Point2D center = mapper.mapToPaneCoordinates(Coordinates.toCartesianSystem(wheelRadius, -angrad));
			itemCircleArray[i].setCenterXY(center);

			itemCircleAngles[i] = toDegrees(angrad);

			scaleTransforms[i].setPivotX(center.getX());
			scaleTransforms[i].setPivotY(center.getY());
		}

		updateScaleOfCircles.invalidated(null);

		rotate.setPivotX(centerX);
		rotate.setPivotY(centerY);

	}

	private DoubleProperty getRotateProperty() {
		DoubleProperty rotProperty = rotate.angleProperty();

		rotProperty.addListener((angle, oldvalue, newvalue) -> {
			for (int i = 0; i < itemCircleArray.length; i++)
				itemCircleArray[i].setRotate(-newvalue.doubleValue());
		});

		return rotProperty;
	}

	/**
	 * positive clockwise direction
	 * 
	 * @param deltaAngle in degrees
	 */
	public void turnWheel(double deltaAngle) {
		angle += deltaAngle;

		directionOfRotation.set(angle > previousAngle ? 1 : -1);

		getRotateProperty().set(angle);

		previousAngle = angle;
	}

	private void fixatePositiveTurn() {
		angle %= 360;
		if (angle < 0)
			angle += 360;
		getRotateProperty().set(angle);
	}

	public void startRotationAnimation(double speedRatio) {
		fixatePositiveTurn();

		Timeline timeline = new Timeline();

		double deltaAngle = 360 / COUNT_OF_CIRCLES_ON_PANE;
		double brakingDistanceInDegrees = directionOfRotation.get() == 1 ? angle + deltaAngle - (angle % deltaAngle)
				: angle < 0 ? angle - deltaAngle - (angle % deltaAngle) : angle - (angle % deltaAngle);

		KeyValue keyValue = new KeyValue(getRotateProperty(), brakingDistanceInDegrees, Interpolator.EASE_OUT);

		EventHandler<ActionEvent> onFinished = t -> angle = getRotateProperty().get() % 360;

		KeyFrame keyFrame = new KeyFrame(Duration.millis(700), onFinished, keyValue);

		timeline.getKeyFrames().add(keyFrame);
		timeline.play();
	}

}
