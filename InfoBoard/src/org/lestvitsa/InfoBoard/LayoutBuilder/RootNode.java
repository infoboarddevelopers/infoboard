package org.lestvitsa.InfoBoard.LayoutBuilder;

import java.util.ListIterator;
import java.util.Stack;
import java.util.function.Consumer;

import org.lestvitsa.InfoBoard.InfoModel.Item;
import org.lestvitsa.InfoBoard.InfoModel.ItemsListIteratorProvider;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

public class RootNode extends BorderPane {

	private Button backButton;

	private AnchorPane bottomPane = new AnchorPane();

	private StackPane centerStackPane = new StackPane();

	private Button firstPageButton;

	private Button nextPageButton;

	private Button lastPageButton;

	private Button prevPageButton;

	private Label nameLabel;

	private ProgressIndicator progressIndicator = new ProgressIndicator();

	private HBox navigatorPane = new HBox();
	private Stack<Node> nodesStack = new Stack<Node>();
	private Label pageNumberLabel;

	RootNode() {

		setCenter(centerStackPane);

		backButton = new Button();
		Region backButtonIcon = new Region();
		backButtonIcon.getStyleClass().add("backButtonIcon");
		backButton.setGraphic(backButtonIcon);
		backButton.setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (!event.isSynthesized()) {
					System.out.println("Back button action");
					Platform.runLater(() -> {
						popNode();
					});
				}
			}
		});

		firstPageButton = new Button();
		Region firstPageButtonIcon = new Region();
		firstPageButtonIcon.getStyleClass().add("firstPageButtonIcon");
		firstPageButton.setGraphic(firstPageButtonIcon);
		firstPageButton.setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (!event.isSynthesized()) {
					goToPage(1);
				}
			}
		});
		prevPageButton = new Button();
		Region prevPageButtonIcon = new Region();
		prevPageButtonIcon.getStyleClass().add("prevPageButtonIcon");
		prevPageButton.setGraphic(prevPageButtonIcon);
		prevPageButton.setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (!event.isSynthesized()) {
					goToPage(getPageable().getCurrentPage() - 1);
				}
			}
		});
		nextPageButton = new Button();
		Region nextPageButtonIcon = new Region();
		nextPageButtonIcon.getStyleClass().add("nextPageButtonIcon");
		nextPageButton.setGraphic(nextPageButtonIcon);
		nextPageButton.setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (!event.isSynthesized()) {
					goToPage(getPageable().getCurrentPage() + 1);
				}
			}
		});
		lastPageButton = new Button();
		Region lastPageButtonIcon = new Region();
		lastPageButtonIcon.getStyleClass().add("lastPageButtonIcon");
		lastPageButton.setGraphic(lastPageButtonIcon);
		lastPageButton.setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (!event.isSynthesized()) {
					goToPage(getPageable().getPageCount());
				}
			}
		});

		pageNumberLabel = new Label("");
		pageNumberLabel.setTextAlignment(TextAlignment.CENTER);
		pageNumberLabel.setAlignment(Pos.CENTER);

		nameLabel = new Label("");
		nameLabel.setTextAlignment(TextAlignment.CENTER);
		nameLabel.setAlignment(Pos.CENTER);

		progressIndicator.setVisible(false);

		AnchorPane.setRightAnchor(nameLabel, 10.0);
		AnchorPane.setLeftAnchor(nameLabel, 10.0);
		AnchorPane.setTopAnchor(nameLabel, 6.0);
		AnchorPane.setBottomAnchor(nameLabel, 6.0);

		AnchorPane.setLeftAnchor(backButton, 10.0);
		AnchorPane.setTopAnchor(backButton, 6.0);
		AnchorPane.setBottomAnchor(backButton, 6.0);

		AnchorPane.setLeftAnchor(navigatorPane, 0.0);
		AnchorPane.setRightAnchor(navigatorPane, 0.0);

		AnchorPane.setRightAnchor(progressIndicator, 6.0);
		AnchorPane.setTopAnchor(progressIndicator, 6.0);
		AnchorPane.setBottomAnchor(progressIndicator, 6.0);

		navigatorPane.setAlignment(Pos.CENTER);

		navigatorPane.getChildren().addAll(firstPageButton, prevPageButton, pageNumberLabel, nextPageButton,
				lastPageButton);

		bottomPane.getChildren().addAll(navigatorPane, nameLabel, progressIndicator, backButton);
		bottomPane.setId("bottomPane");
	}

	public Item getCurrentItem() {
		Node node = nodesStack.peek();
		Item item = (Item) Item.getFromNode(node);
		return item;
	}

	private Item getItem(int offset) {
		if (nodesStack.size() > 1) {
			Node node = nodesStack.peek();
			Item nodeItem = (Item) Item.getFromNode(node);
			Item item = nodeItem;

			if (offset == 0) {
				return item;
			}

			Node parentNode = nodesStack.elementAt(nodesStack.size() - 2);
			Item parentItem = (Item) Item.getFromNode(parentNode);
			if (parentItem instanceof ItemsListIteratorProvider) {
				ListIterator<Item> listIterator = ((ItemsListIteratorProvider) parentItem).getIteratorForItem(item);

				if (offset > 0) {
					listIterator.next();
					while (offset > 0 && listIterator.hasNext()) {
						item = listIterator.next();
						offset--;
					}
				} else {
					while (offset < 0 && listIterator.hasPrevious()) {
						item = listIterator.previous();
						offset++;
					}
				}

				if (item != nodeItem) {
					return item;
				}
			}
		}
		return null;
	}

	public Item getNextItem() {
		return getItem(1);
	}

	protected Pageable getPageable() {
		return (Pageable) getCurrentItem();
	}

	public Item getPreviousItem() {
		return getItem(-1);
	}

	protected void goToPage(int i) {
		if (getCurrentItem() instanceof Pageable) {
			Region container = (Region) nodesStack.peek();
			getPageable().setPrefferedDimensions((int) container.getWidth() - 5, (int) container.getHeight() - 5);

			getPageable().setCurrentPage(i);
			updateBottomPanel();
		}
	}

	void popNode() {
		if (nodesStack.size() > 1) {
			Node dead = nodesStack.peek();
			centerStackPane.getChildren().remove(dead);
			if (centerStackPane.getChildren().size() > 0) {
				Node topNode = centerStackPane.getChildren().get(centerStackPane.getChildren().size() - 1);
				topNode.setVisible(true);
				if (topNode instanceof Parent)
					setChildrenVisible((Parent) topNode, true);
			}
			nodesStack.pop();
			updateBottomPanel();
		}
	}

	void pushNode(Node node) {
		nodesStack.push(node);
		if (centerStackPane.getChildren().size() > 0) {
			Node topNode = centerStackPane.getChildren().get(centerStackPane.getChildren().size() - 1);
			topNode.setVisible(false);
			if (topNode instanceof Parent)
				setChildrenVisible((Parent) topNode, false);
		}
		setNewTopNode(node);
	}

	private void setNewTopNode(Node node) {
		centerStackPane.getChildren().add(node);

		Item item = (Item) Item.getFromNode(node);
		if (item instanceof Pageable) {
			Region container = (Region) node;

			InvalidationListener listener = l -> {
				((Pageable) item).setPrefferedDimensions((int) container.getWidth() - 5,
						(int) container.getHeight() - 5);
			};

			container.widthProperty().addListener(listener);
			container.heightProperty().addListener(listener);
		}

		if (node instanceof Region) {
			((Region) node).setPrefSize(getWidth(), getHeight() - navigatorPane.getHeight());
		}

		updateBottomPanel();
	}

	public void switchToNode(Node node, boolean forward) {

		assert centerStackPane.getChildren().size() > 0;

		Node oldNode = centerStackPane.getChildren().get(centerStackPane.getChildren().size() - 1);

		setNewTopNode(node);
		nodesStack.pop();
		nodesStack.push(node);

		double width = getWidth();

		float m = forward ? 1 : -1;

		String isAnimated = "isAnimated";
		KeyFrame start = new KeyFrame(Duration.ZERO, new KeyValue(node.translateXProperty(), m * width),
				new KeyValue(oldNode.translateXProperty(), 0));
		KeyFrame end = new KeyFrame(Duration.seconds(1), new KeyValue(node.translateXProperty(), 0),
				new KeyValue(oldNode.translateXProperty(), m * -width));
		Timeline slide = new Timeline(start, end);
		slide.setOnFinished(e -> {
			centerStackPane.getChildren().remove(oldNode);
			oldNode.getProperties().put(isAnimated, false);

			Item item = (Item) Item.getFromNode(node);
			if (item instanceof Pageable) {
				goToPage(((Pageable) item).getCurrentPage());
			} else {
				updateBottomPanel();
			}
		});
		oldNode.getProperties().put(isAnimated, true);
		slide.play();
	}

	private void setChildrenVisible(Parent parent, boolean visible) {
		Consumer<Node> с = (Node child) -> {
			child.setVisible(visible);
			if (child instanceof Parent)
				setChildrenVisible((Parent) child, visible);
		};
		if (parent instanceof Pane)
			((Pane) parent).getChildren().forEach(с);
		else if (parent instanceof Group)
			((Group) parent).getChildren().forEach(с);
	}

	private void updateBottomPanel() {
		boolean showBackButton = nodesStack.size() > 1;
		Node node = nodesStack.peek();
		Item item = (Item) Item.getFromNode(node);
		boolean showPageNavigator = item != null && (item instanceof Pageable) && getPageable().getPageCount() > 1;
		if (showBackButton || showPageNavigator) {
			setBottom(bottomPane);
		} else {
			setBottom(null);
		}

		Node topNode = centerStackPane.getChildren().get(centerStackPane.getChildren().size() - 1);
		if (topNode instanceof ProgressUIEvents) {
			ReadOnlyBooleanProperty loadIndicator = ((ProgressUIEvents) topNode).progressIndicatorProperty();
			progressIndicator.visibleProperty().bind(loadIndicator);
		}

		backButton.setVisible(showBackButton);
		navigatorPane.setVisible(showPageNavigator);
		navigatorPane.setPadding(new Insets(6, 6, 6, 6));
		navigatorPane.setSpacing(6);

		if (showPageNavigator) {
			Pageable pg = (Pageable) item;
			pageNumberLabel.setText(String.format("%d из %d", pg.getCurrentPage(), pg.getPageCount()));
			firstPageButton.setDisable(pg.getCurrentPage() == 1);
			prevPageButton.setDisable(pg.getCurrentPage() == 1);
			lastPageButton.setDisable(pg.getCurrentPage() == pg.getPageCount());
			nextPageButton.setDisable(pg.getCurrentPage() == pg.getPageCount());
		}

		nameLabel.setText(item.getName());
		nameLabel.setVisible(showBackButton && !showPageNavigator);

	}
}
