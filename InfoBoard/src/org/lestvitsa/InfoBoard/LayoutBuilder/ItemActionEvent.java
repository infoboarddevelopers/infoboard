package org.lestvitsa.InfoBoard.LayoutBuilder;

import javafx.beans.NamedArg;
import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

public class ItemActionEvent extends Event {

	/**
	 * Common supertype for all item action event types.
	 */
	public static final EventType<ItemActionEvent> ANY = new EventType<ItemActionEvent>(Event.ANY, "ITEM_ACTION_ANY");

	public static final EventType<ItemActionEvent> NEXT_ITEM = new EventType<ItemActionEvent>(ItemActionEvent.ANY,
			"ITEM_ACTION_NEXT");

	public static final EventType<ItemActionEvent> OPEN_ITEM = new EventType<ItemActionEvent>(ItemActionEvent.ANY,
			"ITEM_ACTION_OPEN");

	public static final EventType<ItemActionEvent> PREVIOUS_ITEM = new EventType<ItemActionEvent>(ItemActionEvent.ANY,
			"ITEM_ACTION_PREVIOUS");

	/**
	 * Creates new instance of ItemActionEvent.
	 * 
	 * @param eventType Type of the event
	 */
	public ItemActionEvent(final @NamedArg("eventType") EventType<? extends ItemActionEvent> eventType) {
		super(eventType);
	}

	/**
	 * Creates new instance of ItemActionEvent.
	 * 
	 * @param source    Event source
	 * @param target    Event target
	 * @param eventType Type of the event
	 */
	public ItemActionEvent(final @NamedArg("source") Object source, final @NamedArg("target") EventTarget target,
			final @NamedArg("eventType") EventType<? extends ItemActionEvent> eventType) {
		super(source, target, eventType);
	}

	@Override
	public EventType<? extends ItemActionEvent> getEventType() {
		return (EventType<? extends ItemActionEvent>) super.getEventType();
	}

	public String toString() {
		return super.toString() + "[" + this.getEventType() + "]";
	}

}