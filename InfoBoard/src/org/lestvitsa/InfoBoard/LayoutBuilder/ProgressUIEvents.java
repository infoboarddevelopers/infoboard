package org.lestvitsa.InfoBoard.LayoutBuilder;

import javafx.beans.property.ReadOnlyBooleanProperty;

public interface ProgressUIEvents {
	
	ReadOnlyBooleanProperty progressIndicatorProperty();

}
