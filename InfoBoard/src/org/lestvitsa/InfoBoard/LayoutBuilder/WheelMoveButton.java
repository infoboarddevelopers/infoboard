package org.lestvitsa.InfoBoard.LayoutBuilder;

import org.lestvitsa.InfoBoard.InfoModel.LestvitsaPresentationRoot;
import org.lestvitsa.InfoBoard.LayoutBuilder.WheelMoveButton.Direction;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.animation.Transition;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class WheelMoveButton extends Group {
	public static class Tester extends javafx.application.Application {
		public static void main(String[] args) {
			launch(args);
		}

		@Override
		public void start(Stage primaryStage) throws Exception {
			Pane rootPane = new Pane();
			createButtons(rootPane);

			Scene scene = new Scene(rootPane, 800, 600);
			scene.getStylesheets().add(org.lestvitsa.InfoBoard.Application.Application.class
					.getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		}

		public void createButtons(Pane root) {

			WheelMoveButton moveButton = new WheelMoveButton(Direction.LEFT, 100);
			WheelMoveButton moveButton2 = new WheelMoveButton(Direction.RIGHT, 100);

			root.getChildren().addAll(moveButton, moveButton2);

			moveButton.setCenter(200, 200);
			moveButton2.setCenter(450, 200);

		}
	}

	public enum Direction {
		LEFT, RIGHT
	}

	Circle circle = new Circle();
	Arc arc1 = new Arc();
	Arc arc21 = new Arc();
	Arc arc22 = new Arc();
	Group arc2g = new Group(arc21, arc22);

	public WheelMoveButton(Direction d, double radius) {

		String direction = d == Direction.LEFT ? "left.png" : "right.png";

		createElements();
		setRadius(radius);

		String res = LestvitsaPresentationRoot.class.getResource(direction).toExternalForm();
//		System.out.println(res);
		Image image = new Image(res, 300, 300, true, true);
		circle.setFill(new ImagePattern(image));

		getChildren().addAll(circle, arc1, arc2g);
		getStyleClass().add("pressableItem");

	}

	private void createElements() {
		arc1.setStartAngle(0);
		arc1.setLength(275);
		arc1.setStrokeWidth(2);
		arc1.setFill(null);
		arc1.getStyleClass().add("arc");

		arc21.setStartAngle(270);
		arc21.setLength(165);
		arc21.setStrokeWidth(2);
		arc21.setFill(null);
		arc21.getStyleClass().add("arc");

		arc22.setStartAngle(90);
		arc22.setLength(165);
		arc22.setStrokeWidth(2);
		arc22.setFill(null);
		arc22.getStyleClass().add("arc");

	}

	public void setRadius(double radius) {
		arc1.setRadiusX(radius);
		arc1.setRadiusY(radius);

		arc21.setRadiusX(radius + 6);
		arc21.setRadiusY(radius + 6);

		arc22.setRadiusX(radius + 6);
		arc22.setRadiusY(radius + 6);

		circle.setRadius(radius);

	}

	public void setCenter(double x, double y) {
		arc1.setCenterX(x);
		arc1.setCenterY(y);

		arc21.setCenterX(x);
		arc21.setCenterY(y);

		arc22.setCenterX(x);
		arc22.setCenterY(y);

		circle.setCenterX(x);
		circle.setCenterY(y);

		setRotateTransition();
	}

	private void setRotateTransition() {
		RotateTransition rt1 = new RotateTransition(Duration.seconds(5), arc1);
		rt1.setByAngle(-360);
		rt1.setInterpolator(Interpolator.LINEAR);
		rt1.setCycleCount(Animation.INDEFINITE);
		rt1.play();

		RotateTransition rt4 = new RotateTransition(Duration.seconds(5), arc2g);
		rt4.setByAngle(360);
		rt4.setInterpolator(Interpolator.LINEAR);
		rt4.setCycleCount(Animation.INDEFINITE);
		rt4.play();

	}

	public void setEventHandlerOnMouseReleased(CirclesWheel circlesWheel, double deltaAngle,
			boolean isClockwiseRotation) {

		EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (!event.isSynthesized()) { // TUIO synthesizes duplicate mouse events
					Animation animation = new Transition() {
						{
							setCycleDuration(Duration.millis(500));
						}
						private double prevFrac = 0;

						@Override
						protected void interpolate(double frac) {
							circlesWheel.turnWheel((isClockwiseRotation ? 1 : -1) * deltaAngle * (frac - prevFrac));
							prevFrac = frac;
						}
					};
					animation.play();
					event.consume();
				}
			}
		};

		this.setOnMouseReleased(eventHandler);
	}
}