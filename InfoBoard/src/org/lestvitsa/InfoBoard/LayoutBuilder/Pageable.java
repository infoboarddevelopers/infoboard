package org.lestvitsa.InfoBoard.LayoutBuilder;

import javafx.beans.property.IntegerProperty;

public interface Pageable {

	public IntegerProperty currentPageProperty();

	int getCurrentPage();

	int getPageCount();

	void setCurrentPage(int page);

	void setPrefferedDimensions(int width, int height);

}
