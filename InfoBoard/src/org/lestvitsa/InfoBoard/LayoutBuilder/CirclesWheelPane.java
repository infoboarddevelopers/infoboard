package org.lestvitsa.InfoBoard.LayoutBuilder;

import org.lestvitsa.InfoBoard.InfoModel.Item;
import org.lestvitsa.InfoBoard.InfoModel.ItemActionHandler;
import org.lestvitsa.InfoBoard.InfoModel.ListItem;
import org.lestvitsa.InfoBoard.InfoModel.Utils.CoordinateMapper;
import org.lestvitsa.InfoBoard.InfoModel.Utils.Coordinates;
import org.lestvitsa.InfoBoard.LayoutBuilder.WheelMoveButton.Direction;

import static java.lang.Math.PI;
import static java.lang.Math.abs;

import javafx.beans.InvalidationListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public class CirclesWheelPane extends Pane {
	private double deltaAngle;
	private CoordinateMapper mapper = new CoordinateMapper();
	private EventHandler<MouseEvent> onMousePressed;
	private EventHandler<MouseEvent> onMouseDragged;
	private EventHandler<MouseEvent> onMouseReleased;

	private WheelMoveButton buttonL;
	private WheelMoveButton buttonR;

	private final int COUNT_OF_CIRCLES_ON_PANE;
	private CirclesWheel circlesWheel;

	public CirclesWheelPane(final int countOfCirclesOnPane, ListItem listItem, ItemActionHandler actionHandler)
			throws Exception {
		Pane root = this;
		COUNT_OF_CIRCLES_ON_PANE = countOfCirclesOnPane;

		Item[] items = listItem.getItemsArray();
		circlesWheel = new CirclesWheel(COUNT_OF_CIRCLES_ON_PANE, items, actionHandler);

		createMoveHandlers(circlesWheel);

		createMoveButtons();

		root.getChildren().addAll(circlesWheel, buttonL, buttonR);

		InvalidationListener positionUpdater = (l) -> {
			double rootWidth = root.getWidth();
			double rootHeight = root.getHeight();
			double wheelCenterX = rootWidth / 2;
			double wheelCenterY = rootHeight * 0.1;
			double wheelRadius = (rootHeight - wheelCenterY) * 0.7;

			double wheelPerim = 2 * PI * wheelRadius;
			// TODO
			double itemCircleRadius = wheelPerim / (items.length + 6) / 2;

			circlesWheel.updatePositionAndSize(wheelCenterX, wheelCenterY, wheelRadius, itemCircleRadius);

			mapper.setGridCenter(new Point2D(wheelCenterX, wheelCenterY));

			double ins = rootHeight * 0.2;
			buttonL.setCenter(ins, rootHeight - ins);
			buttonR.setCenter(rootWidth - ins, rootHeight - ins);

			double buttonRadius = itemCircleRadius * 0.5;
			buttonL.setRadius(buttonRadius);
			buttonR.setRadius(buttonRadius);

		};

		root.widthProperty().addListener(positionUpdater);
		root.heightProperty().addListener(positionUpdater);
		root.parentProperty().addListener(positionUpdater);

	}

	private void createMoveButtons() {
		buttonL = new WheelMoveButton(Direction.LEFT, 100);
		buttonR = new WheelMoveButton(Direction.RIGHT, 100);
		double deltaAngle = 360 / COUNT_OF_CIRCLES_ON_PANE;
		buttonL.setEventHandlerOnMouseReleased(circlesWheel, deltaAngle, false);
		buttonR.setEventHandlerOnMouseReleased(circlesWheel, deltaAngle, true);
	}

	private void createMoveHandlers(CirclesWheel circlesWheel) {
		final Anchor anchorPoint = new Anchor();
		final BooleanProperty isDragged = new SimpleBooleanProperty(false);

		onMousePressed = new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (!event.isSynthesized()) {
//					System.out.println("MOUSE_PRESSED");
					anchorPoint.mouseAnchorX = event.getSceneX();
					anchorPoint.mouseAnchorY = event.getSceneY();
					event.consume();
				}
			}
		};
		onMouseDragged = new EventHandler<MouseEvent>() {
			private Point2D anchorP;
			private Point2D curP;
			private double anchorA; // angle in degrees
			private double curA;

			@Override
			public void handle(MouseEvent event) {
				if (!event.isSynthesized()) {
//					System.out.println("MOUSE_DRAGGED");

					if (!isDragged.get()) {
						anchorP = mapper.mapToLocalCoordinates(anchorPoint.mouseAnchorX, anchorPoint.mouseAnchorY);
						anchorA = (Math.toDegrees(Coordinates.toPolarSystem(anchorP).angle) + 360) % 360;
					}

					curP = mapper.mapToLocalCoordinates(event.getSceneX(), event.getSceneY());
					curA = (Math.toDegrees(Coordinates.toPolarSystem(curP).angle) + 360) % 360;

					deltaAngle = (abs(curA - anchorA) > 180)
							? (curA >= anchorA) ? curA - 360 - anchorA : curA - (anchorA - 360)
							: curA - anchorA;

					if (abs(deltaAngle) >= 1 || isDragged.get()) {
						circlesWheel.turnWheel(-deltaAngle);
						isDragged.set(true);
						anchorA = curA;
					}
					
					event.consume();
				}
			}
		};
		onMouseReleased = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (!event.isSynthesized() && isDragged.get()) {
//					System.out.println("MOUSE RELEASED");

					circlesWheel.startRotationAnimation(0);

					isDragged.set(false);
					event.consume();
				}
			}

		};

		circlesWheel.addEventFilter(MouseEvent.MOUSE_PRESSED, onMousePressed);
		circlesWheel.addEventFilter(MouseEvent.MOUSE_DRAGGED, onMouseDragged);
		circlesWheel.addEventFilter(MouseEvent.MOUSE_RELEASED, onMouseReleased);
	}

	private static final class Anchor {
		public double mouseAnchorX;
		public double mouseAnchorY;
	}
}
