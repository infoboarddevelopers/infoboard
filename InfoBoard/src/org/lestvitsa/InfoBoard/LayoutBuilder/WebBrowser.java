package org.lestvitsa.InfoBoard.LayoutBuilder;

import java.io.File;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.concurrent.Worker.State;
import javafx.event.Event;
import javafx.event.EventDispatchChain;
import javafx.event.EventDispatcher;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.ScrollBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class WebBrowser extends BorderPane implements ProgressUIEvents {

	String url;
	final WebView webBox = new WebView();

	final WebEngine webEngine = webBox.getEngine();

	WebBrowser(String url) {
		this.url = url;

		// disable context menu (copy option)
		webBox.setContextMenuEnabled(false);

		WebEventDispatcher webEventDispatcher = new WebEventDispatcher(webBox.getEventDispatcher());
		webEngine.getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {

			@Override
			public void changed(ObservableValue<? extends State> observable, State oldValue, State newValue) {
				if (newValue.equals(State.SUCCEEDED)) {
					// dispatch all events
					webBox.setEventDispatcher(webEventDispatcher);
				}
			}

		});

		webBox.getChildrenUnmodifiable().addListener(new ListChangeListener<Node>() {

			private javafx.geometry.Point2D pLimit;
			private double width, height;

			@Override
			public void onChanged(Change<? extends Node> c) {
				pLimit = webBox.localToScene(webBox.getWidth(), webBox.getHeight());
				webBox.lookupAll(".scroll-bar").stream().map(s -> (ScrollBar) s).forEach(s -> {
					if (s.getOrientation().equals(Orientation.VERTICAL)) {
						width = s.getBoundsInLocal().getWidth();
					}
					if (s.getOrientation().equals(Orientation.HORIZONTAL)) {
						height = s.getBoundsInLocal().getHeight();
					}
				});
				// dispatch all events
				webEventDispatcher.setLimit(pLimit.subtract(width, height));
			}
		});

		webEngine.setUserDataDirectory(new File(System.getProperty("user.home") + File.separator
				+ ".org.lestvitsa.InfoBoard" + File.separator + "WebView"));

		webBox.parentProperty().addListener(l -> {
			if (webBox.getParent() != null) {
				System.out.println(String.format("LOAD %s %s %s\n", webBox, webEngine, getUrl()));
				webEngine.load(getUrl());
			} else {
				System.out.println(String.format("CANCEL %s %s %s\n", webBox, webEngine, getUrl()));
				webEngine.getLoadWorker().cancel();
			}
		});

		webEngine.setOnError(event -> System.out.println(event.getMessage()));
		webEngine.setOnAlert(event -> System.out.println(event.getData()));

		StackPane stack = new StackPane();
		stack.getChildren().addAll(webBox);
		setCenter(stack);

		visibleProperty()
				.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
					if (!newValue) {
						webEngine.load(null);
					}
				});
		parentProperty()
				.addListener((ObservableValue<? extends Parent> observable, Parent oldValue, Parent newValue) -> {
					if (newValue == null) {
						webEngine.load(null);
					}
				});
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public ReadOnlyBooleanProperty progressIndicatorProperty() {
		return webEngine.getLoadWorker().runningProperty();
	}

}

class WebEventDispatcher implements EventDispatcher {

	private boolean allowDrag = false;
	private javafx.geometry.Point2D limit;

	private final EventDispatcher oldDispatcher;

	public WebEventDispatcher(EventDispatcher oldDispatcher) {
		this.oldDispatcher = oldDispatcher;
	}

	@Override
	public Event dispatchEvent(Event event, EventDispatchChain tail) {
		if (event instanceof MouseEvent) {
			MouseEvent m = (MouseEvent) event;
			if (event.getEventType().equals(MouseEvent.MOUSE_CLICKED)
					|| event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
				javafx.geometry.Point2D origin = new javafx.geometry.Point2D((int) m.getX(), (int) m.getY());
				allowDrag = !(origin.getX() < limit.getX() && origin.getY() < limit.getY());
			}
			// avoid selection with mouse dragging, allowing dragging the scrollbars
			if (event.getEventType().equals(MouseEvent.MOUSE_DRAGGED)) {
				if (!allowDrag) {
					event.consume();
				}
			}
			// Avoid selection of word, line, paragraph with mouse click
			if (m.getClickCount() > 1) {
				event.consume();
			}
		}
		if (event instanceof KeyEvent && event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
			KeyEvent k = (KeyEvent) event;
			// Avoid copy with Ctrl+C or Ctrl+Insert
			if ((k.getCode().equals(KeyCode.C) || k.getCode().equals(KeyCode.INSERT)) && k.isControlDown()) {
				event.consume();
			}
			// Avoid selection with shift+Arrow
			if (k.isShiftDown() && (k.getCode().equals(KeyCode.RIGHT) || k.getCode().equals(KeyCode.LEFT)
					|| k.getCode().equals(KeyCode.UP) || k.getCode().equals(KeyCode.DOWN))) {
				event.consume();
			}
		}
		return oldDispatcher.dispatchEvent(event, tail);
	}

	public void setLimit(javafx.geometry.Point2D limit) {
		this.limit = limit;
	}
}
