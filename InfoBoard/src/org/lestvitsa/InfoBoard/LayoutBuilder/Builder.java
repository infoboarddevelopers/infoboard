package org.lestvitsa.InfoBoard.LayoutBuilder;

import java.lang.ref.WeakReference;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.controlsfx.control.GridCell;
import org.controlsfx.control.GridView;
import org.jpedal.utils.StringUtils;
import org.lestvitsa.InfoBoard.Application.Application;
import org.lestvitsa.InfoBoard.InfoModel.BorderContainerItem;
import org.lestvitsa.InfoBoard.InfoModel.CustomViewProvider;
import org.lestvitsa.InfoBoard.InfoModel.GridItem;
import org.lestvitsa.InfoBoard.InfoModel.ImageItem;
import org.lestvitsa.InfoBoard.InfoModel.Item;
import org.lestvitsa.InfoBoard.InfoModel.ItemActionHandler;
import org.lestvitsa.InfoBoard.InfoModel.ItemActionHandler.Action;
import org.lestvitsa.InfoBoard.InfoModel.ListItem;
import org.lestvitsa.InfoBoard.InfoModel.VideoItem;
import org.lestvitsa.InfoBoard.InfoModel.WebItem;

import com.sun.javafx.geom.Point2D;

import hs.javafx.carousel.Carousel;
import hs.javafx.carousel.skin.RayCarouselSkin;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Dimension2D;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.effect.Glow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.util.Callback;

@SuppressWarnings({ "restriction" })

public class Builder {

	class ItemCircle extends Group {
		Circle back;

		Circle front;

		ItemCircle(Circle back, Circle front) {
			this.back = back;
			this.front = front;
			getChildren().addAll(back, front);
//			setStyle("-fx-background-color: blue;");
		}

		public void setCenterXY(double x, double y) {
			back.setCenterX(x);
			front.setCenterX(x);
			back.setCenterY(y);
			front.setCenterY(y);
		}

		void setRadius(double r) {
			back.setRadius(r);
			front.setRadius(r - 20);
		}
	}

	public enum ViewMode {
		FULL_VIEW, ICON, PREVIEW
	}

	public static final String DefaultStyleClass = "item";

	private Circle createCircle(final Color color) {
		// create a circle with desired color
		final Stop[] stops = new Stop[] { new Stop(0, Color.rgb(250, 250, 255)), new Stop(1, color) };
		final Circle circle = new Circle(10, new RadialGradient(0, 0, 0.2, 0.3, 1, true, CycleMethod.NO_CYCLE, stops));
		// add a shadow effect
		circle.setEffect(new InnerShadow(7, color.darker().darker()));
		// change a cursor when it is over circle
		circle.setCursor(Cursor.HAND);

		return circle;
	}

	private Node createCirclesPane(ListItem listItem, ItemActionHandler actionHandler) {
		Item[] items = listItem.getItemsArray();

		Pane result = new Pane();
		ItemCircle[] allGroups = new ItemCircle[items.length];

		for (int index = 0; index < items.length; index++) {

			Circle c1 = createCircle(Color.ANTIQUEWHITE);
			c1.getStyleClass().add("pressableItem");

			Circle c2 = createCircle(Color.ANTIQUEWHITE);
			c2.setFill(new ImagePattern(items[index].getIconImage()));

			ItemCircle g = new ItemCircle(c1, c2);
			allGroups[index] = g;
			g.getStyleClass().addAll("pressableItem");
			result.getChildren().add(g);

			final Item itemForIndex = items[index];
			g.setOnMouseReleased(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if (!event.isSynthesized()) { // TUIO synthesizes duplicate mouse events
						System.out.println("Mouse released in item " + itemForIndex);
						actionHandler.handleItemAction(itemForIndex, Action.Open);
						event.consume();
					}
				}
			});
		}

		final ItemCircle[] groups = allGroups;

		InvalidationListener positionUpdater = l -> {

			double width = result.getWidth();
			double height = result.getHeight();

			if (width == 0 || height == 0)
				return;

			double a = width / 2;
			double b = height / 2;
			double perim = Math.PI * (3 * (a + b) - Math.sqrt((3 * a + b) * (a + 3 * b)));
			double minRad = perim / (groups.length + 6) / 2;
			double radius = Math.min(height / 5, minRad);

			final double tstep = 2 * Math.PI / groups.length;
			double[] steps = new double[groups.length];
			for (int ii = 0; ii < steps.length; ii++) {
				steps[ii] = -Math.PI / 2 + ii * tstep;
			}

			Point2D[] points = new Point2D[groups.length];
			boolean finished = true;
			do {
				for (int ii = 0; ii < steps.length; ii++) {
					double t = steps[ii];
					double x = (a - 1.2 * radius) * Math.cos(t) + width / 2;
					double y = (b - 1.2 * radius) * Math.sin(t) + height / 2;
					points[ii] = new Point2D((float) x, (float) y);
				}

				for (int ii = 1; ii < steps.length; ii++) {
					double d1 = Point2D.distance(points[ii - 1].x, points[ii - 1].y, points[ii].x, points[ii].y);
					double d2 = Point2D.distance(points[ii].x, points[ii].y, points[(ii + 1) % steps.length].x,
							points[(ii + 1) % steps.length].y);
					if (d1 / d2 > 1.01) {
						steps[ii] -= tstep * 0.005;
						finished = false;
					} else if (d2 / d1 > 1.01) {
						steps[ii] += tstep * 0.005;
						finished = false;
					} else {
						finished = true;
					}
				}
			} while (!finished);

			for (int ii = 0; ii < steps.length; ii++) {
				ItemCircle g = groups[ii];
				g.setRadius(radius);
				g.setCenterXY(points[ii].x, points[ii].y);
			}
		};

		result.widthProperty().addListener(positionUpdater);
		result.heightProperty().addListener(positionUpdater);
		result.parentProperty().addListener(positionUpdater);

		return result;
	}

	public Node createIconViewFor(Item item, String styleClass) {
		return createViewForImageItem(item, styleClass, true);
	}

	public Region createPresentation(Item rootItem) {
		RootNode root = new RootNode();

		TriFunction<Item, Action, TriFunction, Node> helper = (inItem, action, handler) -> {
			Item item = null;

			if (action == Action.Next) {
				Item nextItem = root.getNextItem();
				if (nextItem != null) {
					item = nextItem;
				}
			} else if (action == Action.Previous) {
				Item previousItem = root.getPreviousItem();
				if (previousItem != null) {
					item = previousItem;
				}
			} else if (root.getCurrentItem() != inItem) {
				item = inItem;
			}

			if (item != null) {
				Node node = createViewFor(item, Builder.ViewMode.FULL_VIEW, (Item anotherItem, Action aAction) -> {
					return (Node) handler.apply(anotherItem, aAction, handler);
				});
				System.out.println(String.format("Action on item %s node %s", item, node));

				if (action == Action.Previous || action == Action.Next) {
					root.switchToNode(node, action == Action.Next);
				} else {
					root.pushNode(node);
				}
				return node;
			}
			return null;
		};

		Node centralNode = createViewFor(rootItem, Builder.ViewMode.FULL_VIEW, (Item item, Action action) -> {
			return helper.apply(item, action, helper);
		});

		root.pushNode(centralNode);

		MediaView background = createBackground("background.mp4");
		background.fitHeightProperty().bind(root.heightProperty());

		return new StackPane(new BorderPane(background), root);
	}

	private MediaView createBackground(String path) {
		String url = Application.class.getResource(path).toExternalForm();
		Media media = new Media(url);
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.setAutoPlay(true);
		mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
		MediaView mediaView = new MediaView(mediaPlayer);
		
		Glow glow = new Glow();
		glow.setLevel(0.5);
		mediaView.setEffect(glow);
		
		mediaView.setId("presentationsBackground");
		return mediaView;
	}

	public Node createPreviewViewFor(Item item, String styleClass) {
		return createViewForImageItem(item, styleClass, true);
	}

	public Node createViewFor(Item item, ViewMode mode, ItemActionHandler actionHandler) {

		Node resultNode = null;

		String styleClass = item.getStyleClass() != null ? item.getStyleClass() : DefaultStyleClass;

		if (item instanceof CustomViewProvider) {
			try {
				resultNode = ((CustomViewProvider) item).createNode(mode, (ListItem) item, actionHandler);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(-1);
			}
		} else {

			switch (mode) {
			case ICON:
				resultNode = createIconViewFor(item, styleClass);
				break;
			case PREVIEW:
				if (item instanceof BorderContainerItem) {
					resultNode = createViewForBorderContainerItem((BorderContainerItem) item, mode, actionHandler,
							styleClass);
					break;
				} else if (item.hasPreview()) {
					resultNode = createPreviewViewFor(item, styleClass);
					break;
				} else {
					mode = ViewMode.FULL_VIEW;
				}
			case FULL_VIEW:
			default:
				if (item instanceof ListItem) {
					resultNode = createViewForListItem((ListItem) item, actionHandler);
				} else if (item instanceof WebItem) {
					resultNode = createViewForWebItem((WebItem) item);
				} else if (item instanceof VideoItem) {
					resultNode = createViewForVideoItem((VideoItem) item);
				} else if (item instanceof ImageItem) {
					resultNode = createViewForImageItem((ImageItem) item, styleClass, false);
				} else if (item instanceof BorderContainerItem) {
					resultNode = createViewForBorderContainerItem((BorderContainerItem) item, mode, actionHandler,
							styleClass);
				} else if (item instanceof GridItem) {
					resultNode = createViewForGridItem((GridItem) item, actionHandler);
				} else {
					resultNode = createViewForUnknown(item, styleClass, (item instanceof Pageable) ? false : true);
				}
			}
		}

		if (actionHandler != null) {
			installActionHandlerOn(resultNode, item, actionHandler, mode == ViewMode.ICON || mode == ViewMode.PREVIEW);
		}

		item.setToNode(resultNode);

		return resultNode;
	}

	private Node createViewForBorderContainerItem(BorderContainerItem item, ViewMode mode,
			ItemActionHandler actionHandler, String styleClass) {
		Pane result = new AnchorPane();

		Node subView = (mode == Builder.ViewMode.PREVIEW && item.hasPreview()) ? createPreviewViewFor(item, styleClass)
				: createViewFor(item.getSubItem(), Builder.ViewMode.FULL_VIEW, actionHandler);
		result.getChildren().add(subView);

		InvalidationListener subItemUpdater = l -> {

			double width = result.getWidth();
			double height = result.getHeight();

			if (width == 0 || height == 0)
				return;
			Rectangle2D fullViewMargins = item.getFullViewMargins();
			AnchorPane.setTopAnchor(subView, fullViewMargins.getMinX() * height / 100);
			AnchorPane.setRightAnchor(subView, fullViewMargins.getMaxY() * width / 100);
			AnchorPane.setBottomAnchor(subView, fullViewMargins.getMaxX() * height / 100);
			AnchorPane.setLeftAnchor(subView, fullViewMargins.getMinY() * width / 100);
		};

		result.widthProperty().addListener(subItemUpdater);
		result.heightProperty().addListener(subItemUpdater);
		result.parentProperty().addListener(subItemUpdater);

//		result.setStyle("-fx-background-color: #AA0000;");
		return result;
	}

	private Node createViewForGridItem(GridItem gridItem, ItemActionHandler actionHandler) {

		Item[] items = gridItem.getItemsArray();
		List<Item> cList = new ArrayList<Item>(Arrays.asList(items));
		ObservableList<Item> list = FXCollections.observableList(cList);

		GridView<Item> resultGrid = new GridView<>(list);

		resultGrid.cellWidthProperty().set(gridItem.getCellWidth());
		resultGrid.cellHeightProperty().set(gridItem.getCellHeight());

		class CustomGridCell extends GridCell<Item> {
			public CustomGridCell() {
				getStyleClass().add("gridCell");
			}

			@Override
			public void updateItem(Item item, boolean empty) {
				if (empty || item == null) {
					setText(null);
					setGraphic(null);
				} else {
					Dimension2D s;
					Node n = createViewFor(item, ViewMode.PREVIEW, actionHandler);
					// n.setStyle("-fx-background-color: #FFAAAA;");

					if (n instanceof Region) {
						Region r = (Region) n;
						r.setPrefSize(gridItem.getCellWidth() - 5, gridItem.getCellHeight() - 5);
						// System.out.printf("[%f,%f]\n", r.getPrefHeight(), r.getPrefWidth());

						Platform.runLater(() -> {
							r.requestLayout();
						});
					}

					setGraphic(n);
				}
			}
		}
		;

		resultGrid.setCellFactory(gridView -> new CustomGridCell());

		return resultGrid;
	}

	private Node createViewForImage(ObjectProperty<Image> image, Item item, String styleClass, boolean showName) {

		ImageView imageView = new ImageView();
		Bindings.bindBidirectional(imageView.imageProperty(), image);
		imageView.setPreserveRatio(true);
		imageView.setSmooth(true);
		imageView.setCache(true);

		imageView.getStyleClass().add(styleClass);

		BorderPane pane = new BorderPane();

		pane.setOnMouseReleased(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				if (!event.isSynthesized()) {
					if (!imageView.getBoundsInParent().contains(event.getX(), event.getY())) {
						ItemActionEvent actionEvent = null;
						if (event.getX() < pane.getWidth() / 2) {
							actionEvent = new ItemActionEvent(ItemActionEvent.PREVIOUS_ITEM);
						} else {
							actionEvent = new ItemActionEvent(ItemActionEvent.NEXT_ITEM);
						}

						pane.fireEvent(actionEvent);
						event.consume();
					}
				}
			}
		});

		imageView.setOnMouseReleased(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				if (!event.isSynthesized()) {
					ItemActionEvent actionEvent = new ItemActionEvent(ItemActionEvent.OPEN_ITEM);
					pane.fireEvent(actionEvent);
					event.consume();
				}
			}
		});

		installSwipeHadlers(imageView);

		if (showName && item.getName() != null) {
			Label label = new Label(item.getName());
			label.setPrefWidth(10000);
			label.setAlignment(Pos.CENTER);
			label.getStyleClass().add(styleClass);
			// label.setStyle("-fx-background-color: #AAFFAA;");
			pane.setBottom(label);
			imageView.fitHeightProperty().bind(pane.prefHeightProperty().subtract(label.heightProperty()));
		} else {
			imageView.fitHeightProperty().bind(pane.prefHeightProperty());
		}
		imageView.fitWidthProperty().bind(pane.prefWidthProperty());

		if (image.getValue() == null) {
			ProgressIndicator progress = new ProgressIndicator();
			pane.setCenter(progress);

			InvalidationListener listener = new InvalidationListener() {
				@Override
				public void invalidated(Observable observable) {
					pane.setCenter(imageView);
					imageView.imageProperty().removeListener(this);
				}

			};
			imageView.imageProperty().addListener(listener);
		} else {
			pane.setCenter(imageView);
		}

		pane.getStyleClass().add(DefaultStyleClass);

		if (item instanceof Pageable) {
			installPageActions(imageView, item);
		}

		return pane;
	}

	private Node createViewForImageItem(Item item, String styleClass, boolean showName) {
		ObjectProperty<Image> imageProperty = item.getImage();
		if (imageProperty == null) {
			imageProperty = new SimpleObjectProperty<>();
			Image image = item.getIconImage();
			if (image == null) {
				image = item.getDefaultIcon();
			}
			imageProperty.set(image);
		}

		return createViewForImage(imageProperty, item, styleClass, showName);
	}

//	@SuppressWarnings("restriction")
	private Node createViewForListItem(ListItem listItem, ItemActionHandler actionHandler) {
		switch (listItem.getMode()) {
		default:
		case HORIZONTAL_LIST: {
			HBox hPane = new HBox();
			hPane.setAlignment(Pos.CENTER);
			hPane.setFillHeight(true);

//			hPane.setStyle("-fx-background-color: #FFFFAA;");

			double aspectSum = 0;
			for (Item item : listItem.getItems()) {
				Node itemNode = createViewFor(item, ViewMode.PREVIEW, actionHandler);

				HBox.setHgrow(itemNode, Priority.ALWAYS);

				hPane.getChildren().add(itemNode);

				aspectSum += item.getAspectRatio();
			}

			final double aspectSumFinal = aspectSum;

			InvalidationListener updater = l -> {
				for (Node itemNode : hPane.getChildren()) {
					Item item = (Item) Item.getFromNode(itemNode);
					double aspect = item.getAspectRatio();
					((Region) itemNode).setMaxWidth(hPane.getWidth() * aspect / aspectSumFinal);
					((Region) itemNode).setMaxHeight(hPane.getWidth() / aspect);
					((Region) itemNode).requestLayout();
				}
			};

			hPane.widthProperty().addListener(updater);
			hPane.heightProperty().addListener(updater);
			hPane.parentProperty().addListener(updater);

			return hPane;
		}
		case VERTICAL_LIST: {
			VBox vPane = new VBox();
			vPane.setAlignment(Pos.CENTER);

//			vPane.setStyle("-fx-background-color: #FFAAFF;");

			for (Item item : listItem.getItems()) {
				Node itemNode = createViewFor(item, ViewMode.PREVIEW, actionHandler);

				VBox.setVgrow(vPane, Priority.ALWAYS);
				((Region) itemNode).setMaxHeight(Double.MAX_VALUE);

				vPane.getChildren().add(itemNode);
			}
			return vPane;
		}
		case CIRCLES_FLOWER: {
			return createCirclesPane(listItem, actionHandler);
		}
		case PAGINATOR: {
			Item[] items = listItem.getItemsArray();
			Pagination pagination = new Pagination(items.length);
			ObservableList<String> styleClass = pagination.getStyleClass();
			styleClass.add(Pagination.STYLE_CLASS_BULLET);
			pagination.setStyle("-fx-page-information-visible:false;");
			pagination.setPageFactory((Integer pageIndex) -> {
				Node n = createViewFor(items[pageIndex], ViewMode.PREVIEW, actionHandler);
				return n;
			});
			return pagination;
		}
		case VERTICAL_SIDEBAR: {
			Item[] items = listItem.getItemsArray();

			AnchorPane result = new AnchorPane();

			VBox sideBar = new VBox();
			sideBar.setAlignment(Pos.CENTER_RIGHT);
			sideBar.getStyleClass().add("sidebarPane");

			BorderPane itemPane = new BorderPane();

			itemPane.addEventHandler(ItemActionEvent.ANY, event -> {
				System.out.println("Consume action " + event);
				event.consume();
			});

			AnchorPane.setTopAnchor(sideBar, 0.0);
			AnchorPane.setRightAnchor(sideBar, 0.0);
			AnchorPane.setBottomAnchor(sideBar, 0.0);

			ToggleGroup group = new ToggleGroup();
			for (int i = 0; i < items.length; i++) {

				Item iItem = items[i];
				String listName = iItem.getName() == null ? String.format("Раздел %d", i + 1)
						: StringUtils.abbreviate(iItem.getName(), listItem.getAbbreviateLimit());
				ToggleButton b = new ToggleButton(listName);

				b.getStyleClass().add("button");
				sideBar.getChildren().add(b);

				b.setToggleGroup(group);
				b.setOnMouseReleased(new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent event) {
						if (!event.isSynthesized()) {
							Node node = createViewFor(iItem, ViewMode.FULL_VIEW,
									/* iItem instanceof Pageable ? */ actionHandler /* : null */);
							if (iItem instanceof Pageable) {
								((Pageable) iItem).setPrefferedDimensions((int) itemPane.getWidth() - 5,
										(int) itemPane.getHeight() - 5);
							}
							if (node instanceof Region) {
								((Region) node).prefWidthProperty()
										.bind(result.widthProperty().subtract(sideBar.getWidth()));
								((Region) node).prefHeightProperty().bind(result.heightProperty());
							}
							itemPane.setCenter(node);
						}
					}
				});
			}
			group.selectedToggleProperty()
					.addListener((ObservableValue<? extends Toggle> observable, Toggle old, Toggle now) -> {
						if (now == null) {
							group.selectToggle(old);
						}
					});

			AnchorPane.setTopAnchor(itemPane, 0.0);
			AnchorPane.setLeftAnchor(itemPane, 0.0);
			AnchorPane.setBottomAnchor(itemPane, 0.0);

			sideBar.widthProperty().addListener(l -> {
				AnchorPane.setRightAnchor(itemPane, sideBar.getWidth());
			});

			result.getChildren().addAll(sideBar, itemPane);

			Platform.runLater(() -> {
				ToggleButton b = (ToggleButton) sideBar.getChildren().get(0);
				b.fire();
			});

			return result;
		}

		case HORIZONTAL_CAROUSEL: {
			Item[] items = listItem.getItemsArray();

			BorderPane result = new BorderPane();
//			result.setStyle("-fx-background-color: #111111;");
			result.getStyleClass().add("carousel");

			final class ImageHandle {
				private ImageView imageView;
				private final Item item;

				public ImageHandle(Item item) {
					this.item = item;
					imageView = null;
				}

				public ImageView getImage() {
					if (imageView == null) {
						imageView = new ImageView();
						Bindings.bindBidirectional(imageView.imageProperty(), item.getImage());
					}
					return imageView;
				}
			}

			Carousel<ImageHandle> c = new Carousel<ImageHandle>();
			TreeItem<ImageHandle> root = new TreeItem<>();

			RayCarouselSkin skin = new RayCarouselSkin<>(c);

			skin.setMaxCellWidth(1000);
			skin.setMaxCellHeight(700);
			skin.radiusRatioProperty().setValue(0.5);
			skin.viewDistanceRatioProperty().setValue(2.0);
			skin.densityProperty().setValue(1.85);
			skin.carouselViewFractionProperty().setValue(1);
			skin.viewAlignmentProperty().setValue(0.0);
			skin.cellAlignmentProperty().setValue(1.0);

			c.setSkin(skin);

			c.setRoot(root);
			c.setShowRoot(false);

			for (Item item : items) {
				root.getChildren().add(new TreeItem<>(new ImageHandle(item)));
			}

			c.setCellFactory(new Callback<TreeView<ImageHandle>, TreeCell<ImageHandle>>() {

				@Override
				public TreeCell<ImageHandle> call(final TreeView<ImageHandle> carousel) {

					class CustomTreeCell extends TreeCell<ImageHandle> {

						private WeakReference<Item> itemForAction = null;

						public WeakReference<Item> getItemForAction() {
							return itemForAction;
						}

						@Override
						protected void updateItem(ImageHandle item, boolean empty) {
							super.updateItem(item, empty);

							if (!empty) {

								itemForAction = new WeakReference<Item>(item.item);
								ImageView image = item.getImage();
								image.setPreserveRatio(true);
								setGraphic(image);
							} else {

								itemForAction = null;

								setGraphic(null);
							}
						}
					}

					CustomTreeCell carouselCell = new CustomTreeCell();

					carouselCell.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

//					carouselCell.setOnMousePressed(new EventHandler<MouseEvent>() {
//						public void handle(MouseEvent event) {
//							if (!event.isSynthesized() && carouselCell.isSelected()) {
//								event.consume();
//								TreeItem<ImageHandle> selectedItem = c.getSelectionModel().getSelectedItem();
//								System.out.println("SELECTED " + ((TreeItem<ImageHandle>)selectedItem).getValue().item);
//								
//								Item item = carouselCell.getItemForAction().get();
//								System.out.println("CURRENT " + item);
//								if (item != null) {
//									Node nodeForItem = actionHandler.handleItemAction(item, Action.Open);
//									nodeForItem.getStyleClass().add("carouselItem");
//								}
//							}
//						}
//					});

					return carouselCell;
				}
			});

			result.setCenter(c);

			return result;
		}

		}
	}

	public Node createViewForUnknown(Item item, String styleClass, boolean showName) {
		return createViewForImageItem(item, styleClass, showName);
	}

	private MediaControl createViewForVideoItem(VideoItem item) {
		URI location = item.getVideoSource().getURI();
		System.out.println(location.toString());
		Media media = new Media(location.toString());
		MediaPlayer mediaPlayer = new MediaPlayer(media);
//		mediaPlayer.setAutoPlay(true);

		MediaControl mediaControl = new MediaControl(mediaPlayer);

		installSwipeHadlers(mediaControl);

		return mediaControl;
	}

	private Node createViewForWebItem(WebItem item) {
		final WebBrowser webBox = new WebBrowser(item.getUrl());

		webBox.getStyleClass().add("root");

		return webBox;
	}

	private void installActionHandlerOn(Node node, Item item, ItemActionHandler actionHandler, boolean preview) {
		ItemActionHandler filteredActionHandler = (Item i, Action a) -> {
			Boolean isAnimated = (Boolean) node.getProperties().get("isAnimated");
			if (isAnimated == null || !isAnimated) {
				return actionHandler.handleItemAction(i, a);
			}
			return null;
		};

		node.addEventHandler(ItemActionEvent.ANY, event -> {
			if (!preview && event.getEventType().equals(ItemActionEvent.PREVIOUS_ITEM)) {
				filteredActionHandler.handleItemAction(item, Action.Previous);
			} else if (!preview && event.getEventType().equals(ItemActionEvent.NEXT_ITEM)) {
				filteredActionHandler.handleItemAction(item, Action.Next);
			} else if (event.getEventType().equals(ItemActionEvent.OPEN_ITEM)) {
				filteredActionHandler.handleItemAction(item, Action.Open);
			}

			event.consume();
		});
	}

	private void installPageActions(ImageView imageView, Item item) {
		Pageable pg = (Pageable) item;

		pg.currentPageProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue o, Number oldVal, Number newVal) {
				System.out.println(String.format("%d - %d", oldVal.intValue(), newVal.intValue()));
				Bindings.bindBidirectional(imageView.imageProperty(), item.getImage());
			}
		});
	}

	private void installSwipeHadlers(Node node) {
		node.setOnSwipeRight(new EventHandler<SwipeEvent>() {
			@Override
			public void handle(SwipeEvent event) {
				System.out.println("Switch to previous item");
				event.consume();
				ItemActionEvent actionEvent = new ItemActionEvent(ItemActionEvent.PREVIOUS_ITEM);
				node.fireEvent(actionEvent);
			}
		});

		node.setOnSwipeLeft(new EventHandler<SwipeEvent>() {
			@Override
			public void handle(SwipeEvent event) {
				System.out.println("Switch to next item");
				event.consume();
				ItemActionEvent actionEvent = new ItemActionEvent(ItemActionEvent.NEXT_ITEM);
				node.fireEvent(actionEvent);
			}
		});

	}

}
