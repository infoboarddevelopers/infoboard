package org.lestvitsa.InfoBoard.InfoModel;

import java.util.ListIterator;

public interface ItemsListIteratorProvider {

	ListIterator<Item> getIteratorForItem(Item currentItem);

}
