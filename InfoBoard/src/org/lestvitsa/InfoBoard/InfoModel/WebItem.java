package org.lestvitsa.InfoBoard.InfoModel;

public class WebItem extends ImageItem {
	String url;

	public WebItem() {
	}

	public WebItem(String url, String preview) {
		super(FileSource.Type.ApplicationResource, preview);
		this.url = url;
	}
	
	public WebItem(String url, String preview, String icon) {
		super(FileSource.Type.ApplicationResource, preview);
		this.url = url;
		setResourceIcon(icon);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
