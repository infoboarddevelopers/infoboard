package org.lestvitsa.InfoBoard.InfoModel;

import java.util.ArrayList;
import java.util.Arrays;

import org.lestvitsa.InfoBoard.InfoModel.FileSource.Sort;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class FolderGridItem extends GridItem {

	private FileSource itemsPath;
	private boolean recursive = true;
	private Sort sort = Sort.ByName;

	public FolderGridItem() {
	}

	public FolderGridItem(int cellWidth, int cellHeight, FileSource folderSource, Sort sort, boolean recursive) {
		super(cellWidth, cellHeight, folderSource.createItemsFromFolder(sort, recursive));
		this.itemsPath = folderSource;
		this.recursive = recursive;
	}

	public FolderGridItem(int cellWidth, int cellHeight, FileSource folderSource, String preview, Sort sort,
			boolean recursive) {
		super(cellWidth, cellHeight, folderSource.createItemsFromFolder(sort, recursive), preview);
		this.itemsPath = folderSource;
		this.recursive = recursive;
	}

	public void add(Item item) {
		getItems().add(item);
	}

	@JsonIgnore
	public ArrayList<Item> getItems() {
		if (items == null) {
			if (itemsPath != null) {
				items = new ArrayList<Item>();
				Item[] folderItems = itemsPath.createItemsFromFolder(sort, recursive);
				if (folderItems != null) {
					items.addAll(Arrays.asList(folderItems));
				}
			} else {
				return new ArrayList<Item>();
			}
		}
		return items;
	}

	public FileSource getItemsPath() {
		return itemsPath;
	}

	public Sort getSort() {
		return sort;
	}

	public boolean isRecursive() {
		return recursive;
	}

	@JsonIgnore
	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}

	public void setItemsPath(FileSource itemsPath) {
		this.itemsPath = itemsPath;
	}

	public void setRecursive(boolean recursive) {
		this.recursive = recursive;
	}

	public void setSort(Sort sort) {
		this.sort = sort;
	}
}
