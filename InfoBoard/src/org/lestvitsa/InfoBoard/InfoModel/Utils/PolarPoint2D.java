package org.lestvitsa.InfoBoard.InfoModel.Utils;

import javafx.geometry.Point2D;

public class PolarPoint2D {
	public double radius;
	public double angle;

	public PolarPoint2D(double radius, double angle) {
		this.radius = radius;
		this.angle = angle;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (obj instanceof PolarPoint2D) {
			PolarPoint2D other = (PolarPoint2D) obj;
			return radius == other.radius && angle == other.angle;
		} else
			return false;
	}

	@Override
	public String toString() {
		return String.format("[radius = %f, angle = %f]", radius, angle);
	}
}
