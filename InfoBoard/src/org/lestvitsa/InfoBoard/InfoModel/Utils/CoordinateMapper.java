package org.lestvitsa.InfoBoard.InfoModel.Utils;

import javafx.geometry.Point2D;

public class CoordinateMapper {
	private Point2D gridCenter;

	public CoordinateMapper()
	{
	}
	
	public CoordinateMapper(Point2D center)
	{
		gridCenter = center;
	}
	
	public Point2D getGridCenter() {
		return gridCenter;
	}

	public void setGridCenter(Point2D gridCenter) {
		this.gridCenter = gridCenter;
	}

	public Point2D mapToPaneCoordinates(double x, double y) {
		Point2D result = new Point2D(x, -y).add(gridCenter);
		return result;
	}

	public Point2D mapToPaneCoordinates(Point2D point) {
		return mapToPaneCoordinates(point.getX(), point.getY());
	}

	public Point2D mapToLocalCoordinates(double x, double y) {
		return mapToLocalCoordinates(new Point2D(x, y));
	}

	public Point2D mapToLocalCoordinates(Point2D point) {
		Point2D result = point.subtract(gridCenter);
		return new Point2D(result.getX(), -result.getY());
	}
}