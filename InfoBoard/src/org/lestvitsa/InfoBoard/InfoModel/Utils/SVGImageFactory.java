package org.lestvitsa.InfoBoard.InfoModel.Utils;

import static org.apache.batik.transcoder.SVGAbstractTranscoder.KEY_HEIGHT;
import static org.apache.batik.transcoder.SVGAbstractTranscoder.KEY_WIDTH;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.util.XMLResourceDescriptor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.codecentric.centerdevice.javafxsvg.BufferedImageTranscoder;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

public class SVGImageFactory {

	static private Element getDomElementByIdRecursive(Element element, String desiredId) {
		if (desiredId.equals(element.getAttribute("id")))
			return element;
		for (int i = 0; i < element.getChildNodes().getLength(); i++) {
			org.w3c.dom.Node node = element.getChildNodes().item(i);
			if (node.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				Element child = getDomElementByIdRecursive((Element) node, desiredId);
				if (child != null)
					return child;
			}
		}
		return null;
	}

	private InputStream is;
	private String[] parts;

	public SVGImageFactory(InputStream is) {
		this(is, null);
	}
	
	public SVGImageFactory(InputStream is, String[] parts) {
		this.is = is;
		this.parts = parts;
	}

	public Image getSVGImage(String partId, int width, int height) {

		Image svgImage = null;

		try {
			Document d = new SAXSVGDocumentFactory(XMLResourceDescriptor.getXMLParserClassName()).createDocument(null,
					is);

			if (partId != null) {
				for (String aPartId : parts) {
					if (!partId.equals(aPartId)) {
						Element elementToDelete = getDomElementByIdRecursive(d.getDocumentElement(), aPartId);
						if (elementToDelete != null) {
							elementToDelete.getParentNode().removeChild(elementToDelete);
						}
					}
				}
			}

			BufferedImageTranscoder trans = new BufferedImageTranscoder(BufferedImage.TYPE_INT_ARGB);
			trans.addTranscodingHint(KEY_WIDTH, (float) width);
			trans.addTranscodingHint(KEY_HEIGHT, (float) height);
			trans.transcode(new TranscoderInput(d), null);

			svgImage = SwingFXUtils.toFXImage(trans.getBufferedImage(), null);
		} catch (IOException | TranscoderException e) {
			e.printStackTrace();
		}

		return svgImage;
	}

}
