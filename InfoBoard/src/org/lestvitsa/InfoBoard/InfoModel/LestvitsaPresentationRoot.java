package org.lestvitsa.InfoBoard.InfoModel;

import java.io.InputStream;

import org.lestvitsa.InfoBoard.Application.Application;
import org.lestvitsa.InfoBoard.Application.LestvitsaApplication2;
import org.lestvitsa.InfoBoard.InfoModel.Utils.SVGImageFactory;
import org.lestvitsa.InfoBoard.InfoModel.Utils.Xform;
import org.lestvitsa.InfoBoard.LayoutBuilder.Builder.ViewMode;
import org.lestvitsa.InfoBoard.LayoutBuilder.CirclesWheelPane;
import org.lestvitsa.InfoBoard.LayoutBuilder.ItemCircle;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ObservableValue;
import javafx.scene.CacheHint;
import javafx.scene.Node;
import javafx.scene.PerspectiveCamera;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.effect.Blend;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class LestvitsaPresentationRoot extends ListItem implements CustomViewProvider {

	public static class Tester {
		public static void main(String[] args) {
			LestvitsaApplication2.main(args);
		}
	}

	public LestvitsaPresentationRoot() {
		super();
	}

	public LestvitsaPresentationRoot(Mode mode, Item[] items) {
		super(mode, items);
	}

	public LestvitsaPresentationRoot(Mode mode, Item[] items, String preview) {
		super(mode, items, preview);
	}

	public static ItemCircle createLocketCircle() {
		Circle circle = new Circle();
		circle.setFill(Color.WHITE);

		ItemCircle result = new ItemCircle(circle);
		result.getStyleClass().add("pressableItem");
		return result;
	}

	@Override
	public Node createNode(ViewMode mode, ListItem listItem, ItemActionHandler actionHandler) throws Exception {

		AnchorPane resultPane = new AnchorPane();
		resultPane.setMinSize(0, 0); // Magic to fit content!!!

		// Add background
		ImageView backgroundImage = new ImageView(new Image(getResource("background1.jpg")));
		backgroundImage.fitHeightProperty().bind(resultPane.heightProperty());
		backgroundImage.fitWidthProperty().bind(resultPane.widthProperty());
		AnchorPane.setTopAnchor(backgroundImage, 0.0);
		AnchorPane.setLeftAnchor(backgroundImage, 0.0);
		AnchorPane.setBottomAnchor(backgroundImage, 0.0);
		AnchorPane.setRightAnchor(backgroundImage, 0.0);

		CirclesWheelPane circlesPane = new CirclesWheelPane(9, listItem, actionHandler);
		circlesPane.setCache(true);
		circlesPane.setCacheHint(CacheHint.SPEED);
		AnchorPane.setTopAnchor(circlesPane, 0.0);
		AnchorPane.setLeftAnchor(circlesPane, 0.0);
		AnchorPane.setBottomAnchor(circlesPane, 0.0);
		AnchorPane.setRightAnchor(circlesPane, 0.0);

		SubScene logoScene = createLogo();
		logoScene.widthProperty().bind(resultPane.widthProperty());
		logoScene.heightProperty().bind(resultPane.heightProperty());

		resultPane.getChildren().addAll(backgroundImage, circlesPane, logoScene);

		return resultPane;
	}

	private SubScene createLogo() {
		AnchorPane logoPane = new AnchorPane();
		logoPane.setId("logoPane");

		final int verticalTranslate = -2200;
		final int verticalTextTranslate = 1300;
		final int logoSize = 5000;

		Effect e1 = createShiningGoldEffect();
		Text text1 = createText("Лицей Лествица", e1);
		text1.setTranslateX(-text1.getBoundsInLocal().getWidth() / 2);
		text1.setTranslateY(verticalTextTranslate + verticalTranslate - text1.getBoundsInLocal().getHeight() / 2);
		text1.setTranslateZ(2000);
		AnchorPane.setTopAnchor(text1, 0.0);
		AnchorPane.setLeftAnchor(text1, 0.0);

		final String LOGO_FILE = "lestvitsa-logo-white.svg";
		final String LOGO_PARTS[] = { "logo", "lestvitsa0", "lestvitsa1", "lestvitsa2", "lestvitsa3", "lestvitsa4",
				"lestvitsa5", "lestvitsa6", "lestvitsa7", "school0", "school1", "school2", "school3", "school4" };
		SVGImageFactory imageFactory = new SVGImageFactory(getResource(LOGO_FILE), LOGO_PARTS);
		ImageView logoLestvitsa = new ImageView(imageFactory.getSVGImage("logo", logoSize, logoSize));

		logoLestvitsa.setTranslateX(-logoLestvitsa.getBoundsInLocal().getWidth() / 2);
		logoLestvitsa.setTranslateY(verticalTranslate - logoLestvitsa.getBoundsInLocal().getHeight() / 2);
		logoLestvitsa.setTranslateZ(1500);
		AnchorPane.setTopAnchor(logoLestvitsa, 0.0);
		AnchorPane.setLeftAnchor(logoLestvitsa, 0.0);

		logoPane.getChildren().addAll(text1, logoLestvitsa);

		PerspectiveCamera camera = new PerspectiveCamera(true);
		camera.setCache(true);
		camera.setCacheHint(CacheHint.SPEED);
		camera.setNearClip(-20000);
		camera.setFarClip(1000.0);

		Xform cameraRotateXY = new Xform();
		Xform cameraTranslate = new Xform();
		Xform cameraRotateZ = new Xform();

		cameraRotateXY.getChildren().add(cameraTranslate);
		cameraTranslate.getChildren().add(cameraRotateZ);
		cameraRotateZ.getChildren().add(camera);
		cameraRotateZ.setRotateZ(0.0);

		cameraTranslate.setTranslateZ(-15000);

		final Timeline rotationAnimation2 = new Timeline();
		rotationAnimation2.getKeyFrames()
				.addAll(new KeyFrame(Duration.seconds(6),
						new KeyValue(cameraRotateXY.ry.angleProperty(), -30, Interpolator.EASE_BOTH)),
						new KeyFrame(Duration.seconds(18),
								new KeyValue(cameraRotateXY.ry.angleProperty(), 30, Interpolator.EASE_BOTH)),
						new KeyFrame(Duration.seconds(24),
								new KeyValue(cameraRotateXY.ry.angleProperty(), 0, Interpolator.EASE_BOTH)));
		rotationAnimation2.setCycleCount(Animation.INDEFINITE);
		rotationAnimation2.play();

		cameraRotateXY.rx.angleProperty().set(1);
		final Timeline rotationAnimation3 = new Timeline();
		rotationAnimation3.getKeyFrames()
				.addAll(new KeyFrame(Duration.seconds(1),
						new KeyValue(cameraRotateXY.rx.angleProperty(), -1, Interpolator.EASE_BOTH)),
						new KeyFrame(Duration.seconds(2),
								new KeyValue(cameraRotateXY.rx.angleProperty(), 1, Interpolator.EASE_BOTH)));
		rotationAnimation3.setCycleCount(Animation.INDEFINITE);
		rotationAnimation3.play();

		SubScene subScene = new SubScene(logoPane, 0, 0, true, SceneAntialiasing.BALANCED);
		subScene.setCamera(camera);

		subScene.visibleProperty()
				.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean visible) -> {
					if (visible) {
						rotationAnimation2.play();
						rotationAnimation3.play();
					} else {
						rotationAnimation2.pause();
						rotationAnimation3.pause();
					}
				});

		AnchorPane.setTopAnchor(subScene, 0.0);
		AnchorPane.setLeftAnchor(subScene, 0.0);
		AnchorPane.setBottomAnchor(subScene, 0.0);
		AnchorPane.setRightAnchor(subScene, 0.0);

		subScene.setCache(true);
		subScene.setCacheHint(CacheHint.SPEED);

		return subScene;
	}

	private Effect createShiningGoldEffect() {
		Blend blend = new Blend();
		blend.setMode(BlendMode.MULTIPLY);

		DropShadow ds = new DropShadow();
		ds.setColor(/* Color.rgb(254, 235, 66, 0.3) */Color.web("#8fa876"));
		ds.setOffsetX(5);
		ds.setOffsetY(5);
		ds.setRadius(5);
		ds.setSpread(0.2);

		blend.setBottomInput(ds);

		DropShadow ds1 = new DropShadow();
		ds1.setColor(Color.web(/* "#f13a00" */"#8fa876"));
		ds1.setRadius(10);
		ds1.setSpread(0.2);

		Blend blend2 = new Blend();
		blend2.setMode(BlendMode.MULTIPLY);

		InnerShadow is = new InnerShadow();
		is.setColor(Color.web(/* "#feeb42" */"#8fa876"));
		is.setRadius(9);
		is.setChoke(0.8);
		blend2.setBottomInput(is);

		InnerShadow is1 = new InnerShadow();
		is1.setColor(Color.web(/* "#f13a00" */"#8fa876"));
		is1.setRadius(5);
		is1.setChoke(0.4);
		blend2.setTopInput(is1);

		Blend blend1 = new Blend();
		blend1.setMode(BlendMode.MULTIPLY);
		blend1.setBottomInput(ds1);
		blend1.setTopInput(blend2);

		blend.setTopInput(blend1);
		return blend;
	}

	private Text createText(String text, Effect effect) {
		Text textNode = new Text();
		textNode.setText(text);

		Font f = Application.getFontFromResource("fonts/Izhitsa.ttf", 600);
		textNode.setFont(f);
		textNode.setEffect(effect);
		textNode.setFill(Color.WHITE);

		return textNode;
	}

	InputStream getResource(String resourceName) {
		return getClass().getResourceAsStream(resourceName);
	}

}
