package org.lestvitsa.InfoBoard.InfoModel;

import javafx.beans.property.ObjectProperty;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;

public class BorderContainerItem extends Item {

	private Rectangle2D fullViewMargins;

	private Rectangle2D previewMargins;

	private Item subitem;

	BorderContainerItem() {
	}

	public BorderContainerItem(Item subItem, Rectangle2D previewMargins, Rectangle2D fullViewMargins) {
		this.subitem = subItem;
		this.previewMargins = previewMargins;
		this.fullViewMargins = fullViewMargins;
	}

	public Object getActionItem() {
		return subitem.getActionItem();
	}

	public float getAspectRatio() {
		return subitem.getAspectRatio();
	}

	public Rectangle2D getFullViewMargins() {
		return fullViewMargins;
	}

	public FileSource getIcon() {

		return subitem != null ? subitem.getIconSource() : null;
	}

	public Image getIconImage() {
		return subitem != null ? subitem.getIconImage() : null;
	}

	public ObjectProperty<Image> getImage() {
		return subitem.getImage();
	}

	public Rectangle2D getPreviewMargins() {
		return previewMargins;
	}

	public Item getSubItem() {
		return subitem;
	}

	public void setFullViewMargins(Rectangle2D fullViewMargins) {
		this.fullViewMargins = fullViewMargins;
	}

	public void setIconSource(FileSource icon) {
		subitem.setIconSource(icon);
	}

	public void setPreviewMargins(Rectangle2D previewMargins) {
		this.previewMargins = previewMargins;
	}

	public void setSubItem(Item item) {
		this.subitem = item;
	}

}
