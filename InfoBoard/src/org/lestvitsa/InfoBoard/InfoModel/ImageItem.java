package org.lestvitsa.InfoBoard.InfoModel;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;

public class ImageItem extends Item {

	public static ImageItem createFromResource(String imageResource) {
		return new ImageItem(FileSource.Type.ApplicationResource, imageResource);
	}

	private boolean backgroundLoading = false;

	private ObjectProperty<Image> image = new SimpleObjectProperty<>();
	private FileSource imageSource;

	private boolean preserveRatio = false;

	private double requestedHeight = 0;

	private double requestedWidth = 0;

	private boolean smooth = false;

	public ImageItem() {

	}

	public ImageItem(FileSource.Type mode, String imagePath) {
		imageSource = FileSource.create(mode, imagePath);
	}

	public ImageItem(FileSource.Type mode, String imagePath, double requestedWidth, double requestedHeight,
			boolean preserveRatio, boolean smooth, boolean backgroundLoading) {
		this.imageSource = FileSource.create(mode, imagePath);
		this.backgroundLoading = backgroundLoading;
		this.preserveRatio = preserveRatio;
		this.requestedHeight = requestedHeight;
		this.requestedWidth = requestedWidth;
		this.smooth = smooth;
	}

	@JsonIgnore
	public float getAspectRatio() {
		Image img = imageSource.getImage();
		return (float) (img.getWidth() / img.getHeight());
	}

	@JsonIgnore
	public ObjectProperty<Image> getImage() {

		if (requestedHeight != 0 && requestedWidth != 0) {
			image.set(imageSource.getImage(requestedWidth, requestedHeight, preserveRatio, smooth, backgroundLoading));
		} else {
			image.set(imageSource.getImage());
		}

		return image;
	}

	@JsonGetter("image")
	public FileSource getImageSource() {
		return imageSource;
	}

	@JsonSetter("image")
	public void setImageSource(FileSource imageSource) {
		this.imageSource = imageSource;
	}
}
