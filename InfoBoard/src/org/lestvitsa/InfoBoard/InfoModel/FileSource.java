package org.lestvitsa.InfoBoard.InfoModel;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.lestvitsa.InfoBoard.Application.Application;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.scene.image.Image;

public class FileSource {

	public enum Sort {
		ByModificationDate, ByName
	}

	public enum Type {
		ApplicationResource, LocalPath, PresentationResource
	}

	static final Set<String> HtmlExtensionsSet = Stream.of("html", "htm", "txt").collect(Collectors.toSet());

	static final Set<String> ImageExtensionsSet = Stream.of("jpg", "jpeg", "tif", "tiff", "png", "gif")
			.collect(Collectors.toSet());

	static final Set<String> PDFExtensionsSet = Stream.of("pdf").collect(Collectors.toSet());

	static final Set<String> VideoExtensionsSet = Stream.of("mp4").collect(Collectors.toSet());

	public static FileSource create(Type mode, String sourceString) {
		FileSource res = new FileSource();
		res.setType(mode);
		res.setSource(sourceString);
		return res;
	}

	public static URL getResourceURL(String resource) {
		return Item.class.getResource(resource);
	}

	static boolean hasExtension(String fileName, final Set<String> set) {
		int i = fileName.lastIndexOf('.');
		if (i > 0) {
			String ext = fileName.substring(i + 1).toLowerCase();
			return set.contains(ext);
		}

		return false;
	}

	private Image image;

	private String sourceString;

	private Type sourceType;

	public Item[] createItemsFromFolder(Sort sort, boolean recursive) {
		ArrayList<Item> items = new ArrayList<Item>();
		String folderStr = getPath();
		File dir = new File(folderStr);
		if (!dir.exists()) {
			dir.mkdir();
			return new Item[0];
		}
		File[] directoryListing = dir.listFiles();
		System.out.println("Process folder " + folderStr);
		if (directoryListing != null) {
			if (sort == Sort.ByName) {
				Arrays.sort(directoryListing, new Comparator<File>() {
					@Override
					public int compare(File o1, File o2) {
						return o1.getName().compareTo(o2.getName());
					}
				});
			} else {
				Arrays.sort(directoryListing, new Comparator<File>() {
					@Override
					public int compare(File o1, File o2) {
						long m1 = o1.lastModified();
						long m2 = o2.lastModified();
						return m1 < m2 ? -1 : (m1 > m2 ? 1 : 0);
					}
				});
			}

			for (File child : directoryListing) {
				Item item = null;
				try {
					if (recursive && child.isDirectory()) {
						String localPath = child.toString();
						System.out.println("\tFound FOLDER\t" + localPath);
						item = new FolderGridItem(800, 600, FileSource.create(FileSource.Type.LocalPath, localPath),
								"folder-icon.png", sort, recursive);
					} else if (hasExtension(child.getName(), ImageExtensionsSet)) {
						String localPath = child.toURI().toURL().toString();
						System.out.println("\tFound IMAGE\t" + localPath);
						item = new ImageItem(FileSource.Type.LocalPath, localPath, 1000, 1000, true, true, false);
					} else if (hasExtension(child.getName(), VideoExtensionsSet)) {
						String localPath = child.toString();
						System.out.println("\tFound VIDEO\t" + localPath);
						item = new VideoItem(FileSource.create(FileSource.Type.LocalPath, localPath));
					} else if (hasExtension(child.getName(), PDFExtensionsSet)) {
						String localPath = child.toString();
						System.out.println("\tFound PDF\t" + localPath);
						item = new PDFItem(FileSource.create(FileSource.Type.LocalPath, localPath), true);
					} else if (hasExtension(child.getName(), HtmlExtensionsSet)) {
						String localPath = child.toURI().toURL().toString();
						System.out.println("\tFound HTML\t" + localPath);
						item = new WebItem(localPath, "website.png", "website.png");
					}
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (item != null) {
					String name = child.getName();
					int i = name.lastIndexOf('.');
					if (i > 0) {
						name = name.substring(0, i);
					}
					item.setName(name);
					items.add(item);
				}
			}
		}

		return items.toArray(new Item[0]);
	}

	@JsonIgnore
	public Image getImage() {
		if (image == null) {
			String path = getPath();
			try {
				if (path.endsWith(".pdf")) {
					image = new PDFItem(this, false).getImage().get();
				} else {
					image = new Image(path);
				}
			} catch (Exception ex) {
				System.err.println("Failed to load " + path);
				throw ex;
			}
		}
		return image;
	}

	@JsonIgnore
	public Image getImage(double requestedWidth, double requestedHeight, boolean preserveRatio, boolean smooth,
			boolean backgroundLoading) {
		if (image == null || image.getHeight() != requestedHeight || image.getWidth() != requestedWidth) {
			String path = getPath();
			try {
				image = new Image(path, requestedWidth, requestedHeight, preserveRatio, smooth, backgroundLoading);
			} catch (Exception ex) {
				System.err.println("Failed to load " + path);
				throw ex;
			}
		}
		return image;
	}

	@JsonIgnore
	public String getPath() {
		String path = null;
		switch (sourceType) {
		case ApplicationResource:
			path = getResourceURL(sourceString).toExternalForm();
			break;
		case LocalPath:
			path = sourceString;
			break;
		case PresentationResource:
			Path presentationPath = Paths.get(Application.getCurrentApplication().getPresentationPath()).getParent();
			path = presentationPath.resolve(sourceString).toString();
			break;
		}

		return path;
	}

	public String getSource() {
		return sourceString;
	}

	public Type getType() {
		return sourceType;
	}

	@JsonIgnore
	public URI getURI() {
		if (sourceType == Type.ApplicationResource) {
			try {
				return getResourceURL(sourceString).toURI();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		File f = new File(getPath());
		return f.toURI();
	}

	public void setSource(String sourceString) {
		this.sourceString = sourceString;
	}

	public void setType(Type mode) {
		this.sourceType = mode;
	}

}
