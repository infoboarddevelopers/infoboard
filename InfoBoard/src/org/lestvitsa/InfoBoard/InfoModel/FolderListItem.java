package org.lestvitsa.InfoBoard.InfoModel;

import java.util.ArrayList;
import java.util.Arrays;

import org.lestvitsa.InfoBoard.InfoModel.FileSource.Sort;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class FolderListItem extends ListItem {
	private FileSource itemsPath;
	private boolean recursive = true;
	private Sort sort = Sort.ByName;

	public FolderListItem() {
	}

	public FolderListItem(Mode mode, FileSource folderSource, Sort sort, boolean recursive) {
		super(mode, folderSource.createItemsFromFolder(sort, recursive));
		this.sort = sort;
		this.itemsPath = folderSource;
		this.setRecursive(recursive);
	}

	public FolderListItem(Mode mode, FileSource folderSource, String previewURL, Sort sort, boolean recursive) {
		super(mode, folderSource.createItemsFromFolder(sort, recursive), previewURL);
		this.sort = sort;
		this.itemsPath = folderSource;
		this.setRecursive(recursive);
	}

	public FolderListItem(Mode mode, String itemsPath, Sort sort, boolean recursive) {
		this(mode, FileSource.create(FileSource.Type.LocalPath, itemsPath), sort, recursive);
	}

	public FolderListItem(Mode mode, String itemsPath, String previewURL, Sort sort, boolean recursive) {
		this(mode, FileSource.create(FileSource.Type.LocalPath, itemsPath), previewURL, sort, recursive);
	}

	@JsonIgnore
	public ArrayList<Item> getItems() {
		if (items == null) {
			if (itemsPath != null) {
				items = new ArrayList<Item>();
				Item[] folderItems = itemsPath.createItemsFromFolder(sort, recursive);
				if (folderItems != null) {
					items.addAll(Arrays.asList(folderItems));
				}
			} else {
				return new ArrayList<Item>();
			}
		}
		return items;
	}

	public FileSource getItemsPath() {
		return itemsPath;
	}

	public FileSource.Sort getSort() {
		return sort;
	}

	public boolean isRecursive() {
		return recursive;
	}

	@JsonIgnore
	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}

	public void setItemsPath(FileSource itemsPath) {
		this.itemsPath = itemsPath;
	}

	public void setRecursive(boolean recursive) {
		this.recursive = recursive;
	}

	public void setSort(FileSource.Sort sort) {
		this.sort = sort;
	}
}
