package org.lestvitsa.InfoBoard.InfoModel;

import org.lestvitsa.InfoBoard.LayoutBuilder.Builder.ViewMode;

import javafx.scene.Node;

public interface CustomViewProvider {

	Node createNode(ViewMode mode, ListItem listItem, ItemActionHandler actionHandler) throws Exception;

}
