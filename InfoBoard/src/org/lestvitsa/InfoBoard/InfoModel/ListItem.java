package org.lestvitsa.InfoBoard.InfoModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ListIterator;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;

public class ListItem extends Item implements ItemsListIteratorProvider {

	public enum Mode {
		CIRCLES_FLOWER, HORIZONTAL_CAROUSEL, HORIZONTAL_LIST, PAGINATOR, VERTICAL_LIST, VERTICAL_SIDEBAR
	}

	private int abbreviateLimit = 32;
	protected ArrayList<Item> items;

	private Mode mode = Mode.HORIZONTAL_LIST;
	private ObjectProperty<Image> preview = new SimpleObjectProperty<>();

	private FileSource previewSource;

	public ListItem() {
	}

	public ListItem(Mode mode, Item[] items) {
		this.mode = mode;
		Collections.addAll(getItems(), items);
	}

	public ListItem(Mode mode, Item[] items, String preview) {
		this(mode, items);
		this.previewSource = FileSource.create(FileSource.Type.ApplicationResource, preview);
	}

	public void add(Item item) {
		getItems().add(item);
	}

	public int getAbbreviateLimit() {
		return abbreviateLimit;
	}

	public float getAspectRatio() {
		float result = 0;
		for (Item a : items) {
			result += a.getAspectRatio();
		}
		return result;
	}

	public ObjectProperty<Image> getImage() {
		if (previewSource != null) {
			preview.set(previewSource.getImage());
		} else {
			preview = super.getImage();
		}
		return preview;
	}

	public ArrayList<Item> getItems() {
		if (items == null) {
			items = new ArrayList<Item>();
		}
		return items;
	}

	@JsonIgnore
	public Item[] getItemsArray() {
		return getItems().toArray(new Item[0]);
	}

	@Override
	public ListIterator<Item> getIteratorForItem(Item currentItem) {
		int index = getItems().indexOf(currentItem);
		if (index >= 0) {
			return getItems().listIterator();
		}
		return null;
	}

	public Mode getMode() {
		return mode;
	}

	@JsonGetter("preview")
	public FileSource getPreviewSource() {
		return previewSource;
	}

	public void setAbbreviateLimit(int abbreviateLimit) {
		this.abbreviateLimit = abbreviateLimit;
	}

	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	@JsonSetter("preview")
	public void setPreviewSource(FileSource previewSource) {
		this.previewSource = previewSource;
	}

}
