package org.lestvitsa.InfoBoard.InfoModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ListIterator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;

public class GridItem extends Item implements ItemsListIteratorProvider {

	private int cellHeight;
	private int cellWidth;
	protected ArrayList<Item> items;

	private FileSource preview;
	private ObjectProperty<Image> previewImage = new SimpleObjectProperty<>();

	public GridItem() {
	}

	public GridItem(int cellWidth, int cellHeight, Item[] items) {
		this.cellWidth = cellWidth;
		this.cellHeight = cellHeight;
		Collections.addAll(getItems(), items);
	}

	public GridItem(int cellWidth, int cellHeight, Item[] items, String preview) {
		this(cellWidth, cellHeight, items);
		this.preview = FileSource.create(FileSource.Type.ApplicationResource, preview);
	}

	public void add(Item item) {
		getItems().add(item);
	}

	public int getCellHeight() {
		return cellHeight;
	}

	public int getCellWidth() {
		return cellWidth;
	}

	public ObjectProperty<Image> getImage() {
		if (preview != null) {
			previewImage.set(preview.getImage());
			return previewImage;
		}

		return super.getImage();
	}

	public ArrayList<Item> getItems() {
		if (items == null) {
			items = new ArrayList<Item>();
		}
		return items;
	}

	@JsonIgnore
	public Item[] getItemsArray() {
		return getItems().toArray(new Item[0]);
	}

	@Override
	public ListIterator<Item> getIteratorForItem(Item currentItem) {
		int index = getItems().indexOf(currentItem);
		if (index >= 0) {
			return getItems().listIterator(index);
		}
		return null;
	}

	public FileSource getPreviewSource() {
		return preview;
	}

	public void setCellHeight(int cellHeight) {
		this.cellHeight = cellHeight;
	}

	public void setGridWidth(int cellWidth) {
		this.cellWidth = cellWidth;
	}

	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}

	public void setPreviewSource(FileSource previewSource) {
		this.preview = previewSource;
	}
}
