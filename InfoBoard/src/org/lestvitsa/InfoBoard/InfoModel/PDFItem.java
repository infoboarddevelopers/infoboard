package org.lestvitsa.InfoBoard.InfoModel;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import org.jpedal.PdfDecoderFX;
import org.jpedal.exception.PdfException;
import org.jpedal.fonts.FontMappings;
import org.lestvitsa.InfoBoard.LayoutBuilder.Pageable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonTypeName("PDFItem")
public class PDFItem extends Item implements Pageable {

	public static PDFItem createFromResource(String pdfResource, boolean asyncLoad) {
		return new PDFItem(pdfResource, asyncLoad);
	}

	private boolean asyncLoad = false;

	private final IntegerProperty currentPage = new SimpleIntegerProperty(1);

	FileSource pdf;

	PdfDecoderFX pdfDecoder;

	private final ObjectProperty<Image> pdfPreview = new SimpleObjectProperty<>();
	private int prefferedHeight = 0;

	private int prefferedWidth = 0;

	{
		String fontsPath = PDFItem.class.getResource("/fonts").getPath();
		FontMappings.setFontDirs(new String[] { fontsPath });
	}

	public PDFItem() {
	}

	public PDFItem(FileSource pdfPath, boolean asyncLoad) {
		this.asyncLoad = asyncLoad;
		setPdf(pdfPath);
	}

	public PDFItem(String pdfPath, boolean asyncLoad) {
		this.asyncLoad = asyncLoad;
		setPdf(FileSource.create(FileSource.Type.ApplicationResource, pdfPath));
	}

	@JsonIgnore
	@Override
	public final IntegerProperty currentPageProperty() {
		return currentPage;
	}

	private void doPrepareDecoder() {
		synchronized (this) {
			if (pdfDecoder == null) {
				try {
					PdfDecoderFX decoder = new PdfDecoderFX();
					URI pdfURI = getPdfURI();
					System.out.println("Open " + pdfURI);
					InputStream is = pdfURI.toURL().openStream();
					decoder.openPdfFileFromInputStream(is, true);
					pdfDecoder = decoder;
					System.out.println("Opened " + pdfURI);

					Runnable r = () -> {
						generatePreview(asyncLoad);
					};

					if (Platform.isFxApplicationThread()) {
						r.run();
					} else {
						Platform.runLater(r);
					}
				} catch (PdfException | IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	protected void finalize() {
		if (pdfDecoder != null) {
			pdfDecoder.closePdfFile();
		}
	}

	private Image generatePreview() {
		int page = getCurrentPage();
		int height = prefferedHeight;
		int width = prefferedWidth;

		int pWidth = width != 0 ? width : pdfDecoder.getPdfPageData().getCropBoxWidth(page);
		int pHeight = height != 0 ? height : pdfDecoder.getPdfPageData().getCropBoxHeight(page);

		return getPageAsImage(page, pWidth, pHeight);
	}

	private void generatePreview(boolean asyncLoadFlag) {
		assert pdfDecoder != null;
		if (asyncLoadFlag) {
			new Thread(() -> {
				Image img = generatePreview();
				Platform.runLater(() -> {
					pdfPreview.set(img);
				});
			}).start();
		} else {
			pdfPreview.set(generatePreview());
		}
	}

	@JsonIgnore
	public float getAspectRatio() {
		return (float) (getImage().get().getWidth() / getImage().get().getHeight());
	}

	@JsonIgnore
	@Override
	public final int getCurrentPage() {
		return currentPage.get();
	}

	@JsonIgnore
	public ObjectProperty<Image> getImage() {
		if (pdfPreview.get() == null) {
			updatePreview();
		}

		return pdfPreview;
	}

	private Image getPageAsImage(int page, int width, int height) {

		BufferedImage img;
		try {

			float scaleX = width / (float) pdfDecoder.getPdfPageData().getCropBoxWidth(page);
			float scaleY = height / (float) pdfDecoder.getPdfPageData().getCropBoxHeight(page);
			System.out.printf("Preview (%d [%2.2f],%d[%2.2f]) %s\n", width, scaleX, height, scaleY, getPdfURI());
			pdfDecoder.setPageParameters(Math.min(scaleX, scaleY), page);
			img = pdfDecoder.getPageAsImage(page);

			Image result = SwingFXUtils.toFXImage(img, null);

			System.out.printf("Generated preview (%3.1f ,%3.1f) for %s\n", result.getWidth(), result.getHeight(),
					getPdfURI());

			return result;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@JsonIgnore
	@Override
	public int getPageCount() {
		if (pdfDecoder != null) {
			return pdfDecoder.getPageCount();
		} else {
			return 1;
		}
	}

	public FileSource getPdf() {
		return pdf;
	}

	@JsonIgnore
	public URI getPdfURI() {
		return pdf.getURI();
	}

	@JsonIgnore
	public PdfDecoderFX initPDFDecoder() {
		assert pdfDecoder == null;

		if (asyncLoad) {
			new Thread(() -> {
				doPrepareDecoder();
			}).start();
		} else {
			doPrepareDecoder();
		}

		return pdfDecoder;
	}

	public boolean isAsyncLoad() {
		return asyncLoad;
	}

	public void setAsyncLoad(boolean asyncLoad) {
		this.asyncLoad = asyncLoad;
	}

	@Override
	public void setCurrentPage(int page) {
		if (page > 0 && page <= getPageCount()) {
			if (page != currentPage.get()) {
				updatePreview();
			}
			currentPage.set(page);
		}
	}

	public void setPdf(FileSource pdfSource) {
		this.pdf = pdfSource;
	}

	@Override
	public void setPrefferedDimensions(int width, int height) {
		if (width > 0 && height > 0 && (prefferedWidth != width || prefferedHeight != height)) {
			prefferedWidth = width;
			prefferedHeight = height;
			updatePreview();
		} else {
			prefferedWidth = width;
			prefferedHeight = height;
		}
	}

	private void updatePreview() {
		if (pdfDecoder == null) {
			initPDFDecoder();
		} else {
			generatePreview(asyncLoad);
		}
	}
}
