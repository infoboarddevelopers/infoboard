package org.lestvitsa.InfoBoard.InfoModel;

import java.io.File;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class VideoItem extends ImageItem {

	MediaPlayer player;
	private ObjectProperty<Image> previewImage = null;

	private FileSource videoSource;

	public VideoItem() {
	}

	public VideoItem(FileSource videoSource) {
		setVideoSource(videoSource);
	}

	public VideoItem(String sourceString) {
		this(FileSource.create(FileSource.Type.ApplicationResource, sourceString));
	}

	private void generateImage(final File videoFile, final double percent) {

		System.out.println("Generate image for " + videoFile.getPath());
		final Media media = new Media(videoFile.toURI().toString());
		player = new MediaPlayer(media);
		player.setVolume(0.0);
		final MediaView mView = new MediaView(player);

		mView.setFitWidth(800);
		mView.setFitHeight(600);
		mView.setPreserveRatio(true);

		player.setOnPaused(new Runnable() {
			@Override
			public void run() {
				System.out.println("OnPaused");
				int width = (int) mView.getBoundsInLocal().getWidth();
				int height = (int) mView.getBoundsInLocal().getHeight();

				SnapshotParameters params = new SnapshotParameters();
				params.setFill(Color.BLACK);

				Canvas canvas = new Canvas(width, height);
				GraphicsContext gc = canvas.getGraphicsContext2D();
				gc.setFill(Color.color(0.8, 0.8, 0.8, 0.5));
				final double radius = 100;
				gc.fillOval(width / 2 - radius, height / 2 - radius, radius * 2, radius * 2);
				double x = width / 2;
				double y = height / 2;
				gc.setFill(Color.color(0.3, 0.3, 0.3, 0.5));
				gc.fillPolygon(
						new double[] { x + (radius - 10) * Math.cos(0),
								x + (radius - 10) * Math.cos(120.0 / 180 * Math.PI),
								x + (radius - 10) * Math.cos(240.0 / 180 * Math.PI) },
						new double[] { y + (radius - 10) * Math.sin(0),
								y + (radius - 10) * Math.sin(120.0 / 180 * Math.PI),
								y + (radius - 10) * Math.sin(240.0 / 180 * Math.PI) },
						3);

				Pane previewNode = new Pane();
				previewNode.setPrefSize(width, height);
				previewNode.getChildren().addAll(mView, canvas);

				WritableImage writebleImage = new WritableImage(width, height);
				previewNode.snapshot(params, writebleImage);
				previewImage.set(writebleImage);
				System.out.println("Image forgenerated for " + videoFile.getPath());

				player.stop();

				player.setOnReady(null);
				player.setOnPlaying(null);
				player.setOnPaused(null);
				mView.setMediaPlayer(null);

			}
		});

		player.setOnPlaying(new Runnable() {
			@Override
			public void run() {
				System.out.println("OnPlaying");
				player.pause();
			}
		});

		player.setOnReady(new Runnable() {
			@Override
			public void run() {
				System.out.println("OnReady");
				player.seek(Duration.millis(player.getTotalDuration().toMillis() * percent / 100));
				player.play();
			}
		});
	}

	public ObjectProperty<Image> getImage() {
		if (previewImage == null) {
			previewImage = new SimpleObjectProperty();
			generateImage(new File(videoSource.getPath()), 5.0);
		}
		return previewImage;
	}

	public FileSource getVideoSource() {
		return videoSource;
	}

	public void setVideoSource(FileSource videoSource) {
		this.videoSource = videoSource;
	}

}
