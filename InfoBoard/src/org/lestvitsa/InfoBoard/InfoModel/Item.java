package org.lestvitsa.InfoBoard.InfoModel;

import java.net.URL;

import org.lestvitsa.InfoBoard.Application.Application;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javafx.beans.property.ObjectProperty;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({ @JsonSubTypes.Type(value = ListItem.class), @JsonSubTypes.Type(value = GridItem.class),
		@JsonSubTypes.Type(value = ImageItem.class), @JsonSubTypes.Type(value = VideoItem.class),
		@JsonSubTypes.Type(value = PDFItem.class), @JsonSubTypes.Type(value = FolderListItem.class),
		@JsonSubTypes.Type(value = FolderGridItem.class), @JsonSubTypes.Type(value = WebItem.class),
		@JsonSubTypes.Type(value = BorderContainerItem.class),
		@JsonSubTypes.Type(value = LestvitsaPresentationRoot.class) })
public class Item {
	private static String ITEM_PROPERTY_NAME = "org.lestvitsa.InfoBoard.InfoModel.Item";
	private static Pane styleProvider = new Pane() {
		{
			Scene scene = new Scene(this);
			scene.getStylesheets().add(Application.class.getResource("application.css").toExternalForm());
		}
	};

	public static Image createIconWithText(Image image, String text) {

		final int width = 600;
		final int height = 600;

		ImageView imageNode = new ImageView(image);

		Rectangle background = new Rectangle();

		// Setting the properties of the rectangle
		background.setX(0.0f);
		background.setY(0.0f);
		background.setWidth(width);
		background.setHeight(height);
		background.getStyleClass().add("frontCircleBackground");

		Text textShape = new Text(text);
		textShape.setTextAlignment(TextAlignment.CENTER);
		textShape.setWrappingWidth(width);

		Font f = Application.getFontFromResource("fonts/Lobster.ttf", 80);
		textShape.setFont(f);
		
		textShape.getStyleClass().add("itemCircleText");

		StackPane pane = new StackPane();
		pane.getChildren().addAll(background, imageNode, textShape);

		StackPane.setAlignment(textShape, Pos.CENTER);
		StackPane.setAlignment(imageNode, Pos.CENTER);
		pane.setAlignment(Pos.CENTER);

		return createImageFromNode(pane, new Rectangle2D(0 /*-width / 2*/, 0 /*-height / 2*/, width, height));
	}

	public static Image createImageFromNode(Pane pane, Rectangle2D veiwport) {
		WritableImage wi;

		SnapshotParameters parameters = new SnapshotParameters();
		parameters.setFill(Color.GREEN);
		parameters.setViewport(veiwport);

		int imageWidth = (int) veiwport.getWidth();
		int imageHeight = (int) veiwport.getHeight();

		wi = new WritableImage(imageWidth, imageHeight);

		styleProvider.getChildren().add(pane);
		pane.snapshot(parameters, wi);

		return wi;
	}

	public static Image createImageFromResource(String imageResource) {
		return new Image(getResourceURL(imageResource).toExternalForm());
	}

	public static Object getFromNode(Node node) {
		return node.getProperties().get(ITEM_PROPERTY_NAME);
	}

	public static URL getResourceURL(String resource) {
		return Item.class.getResource(resource);
	}

	private FileSource iconSource;

	private String name = null;

	private String styleClass;

	@JsonIgnore
	public Object getActionItem() {
		return null;
	}

	@JsonIgnore
	public float getAspectRatio() {
		return 1;
	}

	@JsonIgnore
	public Image getDefaultIcon() {
		return new Image(getResourceURL("DefaultIcon.png").toExternalForm());
	}

	@JsonIgnore
	public Image getIconImage() {
		if (iconSource != null) {
			Image iconImage = iconSource.getImage();
			if (name != null) {
				return createIconWithText(iconImage, name);
			}

			return iconImage;

		} else {
			if (name != null) {
				return createIconWithText(null, name);
			}
			return getDefaultIcon();
		}
	}

	@JsonGetter("icon")
	public FileSource getIconSource() {
		return iconSource;
	}

	@JsonIgnore
	public ObjectProperty<Image> getImage() {
		return null;
	}

	public String getName() {
		return name;
	}

	public String getStyleClass() {
		return styleClass;
	}

	public boolean hasPreview() {
		return  getImage() != null;
	}

	@JsonSetter("icon")
	public void setIconSource(FileSource icon) {
		this.iconSource = icon;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setResourceIcon(String resource) {
		setIconSource(FileSource.create(FileSource.Type.ApplicationResource, resource));
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	public void setToNode(Node node) {
		node.getProperties().put(ITEM_PROPERTY_NAME, this);
	}
}