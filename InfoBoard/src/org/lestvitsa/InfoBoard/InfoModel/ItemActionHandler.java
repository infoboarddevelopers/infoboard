package org.lestvitsa.InfoBoard.InfoModel;

import javafx.scene.Node;

public interface ItemActionHandler {

	enum Action {
		Next, Open, Previous
	}

	public Node handleItemAction(Item item, Action action);

}
