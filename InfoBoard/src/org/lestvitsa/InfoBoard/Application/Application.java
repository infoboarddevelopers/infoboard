package org.lestvitsa.InfoBoard.Application;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.lestvitsa.InfoBoard.InfoModel.FileSource;
import org.lestvitsa.InfoBoard.InfoModel.FileSource.Sort;
import org.lestvitsa.InfoBoard.InfoModel.Utils.RestartApplicationUtil;
import org.lestvitsa.InfoBoard.InfoModel.FolderGridItem;
import org.lestvitsa.InfoBoard.InfoModel.FolderListItem;
import org.lestvitsa.InfoBoard.InfoModel.GridItem;
import org.lestvitsa.InfoBoard.InfoModel.ListItem;
import org.lestvitsa.InfoBoard.LayoutBuilder.Builder;
import org.tuiofx.Configuration;
import org.tuiofx.TuioFX;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Application extends javafx.application.Application {

	private static Application self;

	public static Application getCurrentApplication() {
		return self;
	}

	static public Font getFontFromResource(String resource, int size) {
		InputStream is = getResourceAsStream(resource);
		final Font f = Font.loadFont(is, size);
		return f;
	}

	static public InputStream getResourceAsStream(String resource) {
		return Application.class.getClassLoader().getResourceAsStream(resource);
	}

	public static void main(String[] args) {
		TuioFX.enableJavaFXTouchProperties();
		launch(args);
	}

	private static String readFile(String path, Charset encoding) {
		try {
			byte[] encoded = Files.readAllBytes(Paths.get(path));
			return new String(encoded, encoding);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		return null;
	}

	public Application() {
		self = this;
	}

	public GridItem createGrid(String folderName) {
		GridItem grid = new FolderGridItem(800, 600,
				FileSource.create(FileSource.Type.PresentationResource, folderName), Sort.ByName, true);

		grid.setName(folderName);

		return grid;
	}

	public ListItem createListWithSidebar(String folderName) {
		FileSource source = FileSource.create(FileSource.Type.PresentationResource, folderName);
		ListItem list = new ListItem(ListItem.Mode.VERTICAL_SIDEBAR, source.createItemsFromFolder(Sort.ByName, false),
				null);
		list.setName(folderName);
		return list;
	}

	public ListItem createPhotoGallery(String folderName) {
		Path p = Paths.get(getPresentationPath());
		ListItem list = new FolderListItem(ListItem.Mode.HORIZONTAL_CAROUSEL,
				p.getParent() + File.separator + folderName, Sort.ByName, true);

		list.setName(folderName);

		return list;

	}

	public String getPresentationPath() {
		Parameters params = getParameters();
		Map<String, String> namedParams = params.getNamed();
		String result = namedParams.get("presentation");

		if (result == null) {
			System.err.println(
					"Nothing to present. Command line argument --presentation=<PATH TO PRSENTATION> is required.");
			System.exit(-1);
		}

		return result;
	}

	private void setAccelerators(Stage primaryStage, Scene scene) {
		KeyCombination reopen = new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN);
		Runnable rn = () -> {
			try {
				RestartApplicationUtil.restartApplication(null);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			System.out.println(reopen);
		};
		scene.getAccelerators().put(reopen, rn);

		KeyCombination reloadStyles = new KeyCodeCombination(KeyCode.F5);
		scene.getAccelerators().put(reloadStyles, () -> {
			List<String> stylesheets = scene.getStylesheets();
			List<String> clonedList = stylesheets.stream().collect(Collectors.toList());
			stylesheets.clear();
			stylesheets.addAll(clonedList);
			System.out.println(reloadStyles + " " + stylesheets);
		});

		KeyCombination setFullScreen = new KeyCodeCombination(KeyCode.F2);
		scene.getAccelerators().put(setFullScreen, () -> {
			primaryStage.setFullScreen(true);
		});
	}

	protected void runStageWithRoot(Stage primaryStage, Parent root) {
		root.setId("MainScreen");

		Scene scene = new Scene(root, 1920, 1080);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		setAccelerators(primaryStage, scene);

		TuioFX tuioFX = new TuioFX(primaryStage, Configuration.ioS());
		tuioFX.start();

		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent t) {
				Platform.exit();
				System.exit(0);
			}
		});

		primaryStage.setScene(scene);

		ObservableList<Screen> screens = Screen.getScreens(); // Get list of Screens
		if (screens.size() > 1) {
			Screen screen2 = screens.get(1);
			Rectangle2D bounds = screen2.getVisualBounds();
			primaryStage.setX(bounds.getMinX());
			primaryStage.setY(bounds.getMinY());
		}
		primaryStage.setFullScreen(true);
		primaryStage.show();

	}

	@Override
	public void start(Stage primaryStage) {
		try {

			ObjectMapper mapper = new ObjectMapper();
			mapper.enableDefaultTyping();
			SimpleModule deser = new SimpleModule();
			deser.addDeserializer(Rectangle2D.class, new Rectangle2DDeserializer());
			mapper.registerModule(deser);

			String presentationFile = getPresentationPath();

			String jsonDataString = readFile(presentationFile, Charset.forName("UTF-8"));
			if (jsonDataString == null || jsonDataString.isEmpty()) {
				System.err.println("Failed to load presentation from " + presentationFile);
				System.exit(-1);
			}

			System.out.println("================ Presentation BEGIN ====================");
			System.out.println(jsonDataString);
			System.out.println("================ Presentation END ======================");

			ListItem list = mapper.readValue(jsonDataString, ListItem.class);

			System.out.println("LOADED SUCCESSFULLY");

			Builder b = new Builder();
			Region root = b.createPresentation(list);

			runStageWithRoot(primaryStage, root);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}