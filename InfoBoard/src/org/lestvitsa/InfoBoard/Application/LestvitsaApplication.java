package org.lestvitsa.InfoBoard.Application;

import java.io.FileWriter;

import org.lestvitsa.InfoBoard.InfoModel.Item;
import org.lestvitsa.InfoBoard.InfoModel.ListItem;
import org.lestvitsa.InfoBoard.LayoutBuilder.Builder;
import org.tuiofx.TuioFX;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import javafx.geometry.Rectangle2D;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class LestvitsaApplication extends Application {
	public static void main(String[] args) {
		TuioFX.enableJavaFXTouchProperties();
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		try {

			ListItem section0 = createListWithSidebar("Администрация");
			Item section1 = createGrid("Медицина");
			Item section2 = createGrid("Охрана труда");
			Item section3 = createGrid("Информация для родителей");
			Item section4 = createGrid("Пожарная безопасность");
			Item section5 = createGrid("Объявления");
			Item section6 = createGrid("Наши достижения");
			Item section7 = createGrid("Новости");
			Item section8 = createPhotoGallery("События");

			ListItem list = new ListItem(ListItem.Mode.CIRCLES_FLOWER, new Item[] { section0, section1, section2,
					section3, section4, section5, section6, section7, section8 });

			ObjectMapper mapper = new ObjectMapper();
			mapper.enableDefaultTyping();
			SimpleModule deser = new SimpleModule();
			deser.addDeserializer(Rectangle2D.class, new Rectangle2DDeserializer());
			mapper.registerModule(deser);

			String jsonDataString = mapper.writeValueAsString(list);

			FileWriter w = new FileWriter(getPresentationPath());
			w.write(jsonDataString);
			w.close();

			System.out.println(jsonDataString);

			list = mapper.readValue(jsonDataString, ListItem.class);

			Builder b = new Builder();
			Region root = b.createPresentation(list);

			runStageWithRoot(primaryStage, root);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
