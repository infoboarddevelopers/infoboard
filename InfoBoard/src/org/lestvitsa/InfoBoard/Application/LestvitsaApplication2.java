package org.lestvitsa.InfoBoard.Application;

import java.io.FileWriter;

import org.lestvitsa.InfoBoard.InfoModel.Item;
import org.lestvitsa.InfoBoard.InfoModel.LestvitsaPresentationRoot;
import org.lestvitsa.InfoBoard.InfoModel.ListItem;
import org.lestvitsa.InfoBoard.InfoModel.PDFItem;
import org.lestvitsa.InfoBoard.InfoModel.WebItem;
import org.lestvitsa.InfoBoard.InfoModel.FileSource.Sort;
import org.lestvitsa.InfoBoard.LayoutBuilder.Builder;
import org.tuiofx.TuioFX;

import com.fasterxml.jackson.annotation.JsonCreator.Mode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import de.codecentric.centerdevice.javafxsvg.SvgImageLoaderFactory;
import de.codecentric.centerdevice.javafxsvg.dimension.PrimitiveDimensionProvider;
import javafx.geometry.Rectangle2D;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class LestvitsaApplication2 extends Application {
	public static void main(String[] args) {
		TuioFX.enableJavaFXTouchProperties();
		System.setProperty("prism.lcdtext", "false");
		System.setProperty("prism.text", "t2k");
		SvgImageLoaderFactory.install(new PrimitiveDimensionProvider());
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		try {

			ListItem section0 = createListWithSidebar("Администрация");
			Item section1 = createGrid("Медицина");
			Item section2 = createGrid("Охрана труда");
			Item section3 = createGrid("Информация для родителей");
			Item section4 = createGrid("Пожарная безопасность");
			Item section5 = createGrid("Объявления");
			Item section6 = createGrid("Наши достижения");
			Item section7 = createGrid("Новости");
//			Item section8 = createPhotoGallery("События");
			Item section9 = new ListItem(ListItem.Mode.CIRCLES_FLOWER,
					new Item[] { new WebItem("http://lestvitsa.com/", null, "website.png"),
//							PDFItem.createFromResource("sample3.pdf", false),
							new WebItem("https://www.facebook.com/Lestvitsa", null, "facebook.jpeg"),
							new WebItem("https://www.instagram.com/", null, "instagram.png"), new WebItem(
									"https://www.youtube.com/channel/UCY2k1DOfUKzQog7Z5VKurFw", null, "youtube.png") });
			section9.setName("Социальные сети");

			Item section10 = createGrid("Школьные проекты");
			/*
			 * new ListItem(ListItem.Mode.VERTICAL_SIDEBAR, new Item[] {
			 * createGrid("Концерты"), createPhotoGallery("Мероприятия"),
			 * createGrid("Лагерь") }, null); section10.setName("Школьные проекты");
			 */
			ListItem section11 = createListWithSidebar("Администрация школы");

			ListItem list = new LestvitsaPresentationRoot(ListItem.Mode.CIRCLES_FLOWER,
					new Item[] { section0, section1, section2, section3, section4, section9, section6, section7,
							/* section8, */ section5, section10, section11 });

			/*
			 * ObjectMapper mapper = new ObjectMapper(); mapper.enableDefaultTyping();
			 * SimpleModule deser = new SimpleModule();
			 * deser.addDeserializer(Rectangle2D.class, new Rectangle2DDeserializer());
			 * mapper.registerModule(deser);
			 * 
			 * String jsonDataString = mapper.writeValueAsString(list);
			 * 
			 * FileWriter w = new FileWriter(getPresentationPath());
			 * w.write(jsonDataString); w.close();
			 * 
			 * System.out.println(jsonDataString);
			 * 
			 * list = mapper.readValue(jsonDataString, ListItem.class);
			 */

			Builder b = new Builder();
			Region root = b.createPresentation(list);

			runStageWithRoot(primaryStage, root);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
