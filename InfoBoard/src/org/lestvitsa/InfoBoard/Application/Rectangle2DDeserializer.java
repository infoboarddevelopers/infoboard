package org.lestvitsa.InfoBoard.Application;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import javafx.geometry.Rectangle2D;

class Rectangle2DDeserializer extends JsonDeserializer<Rectangle2D> {

	/**
	 * A place to hold information from the JSON stream temporarily.
	 */
	private static class IntermediateBoundingBox {
		public Double height;
		public Double maxX;
		public Double maxY;
		public Double minX;
		public Double minY;
		public Double width;
	}

	@Override
	public Rectangle2D deserialize(JsonParser jp, DeserializationContext arg1) throws IOException {

		IntermediateBoundingBox bbox = jp.readValueAs(IntermediateBoundingBox.class);

		if (bbox.minX == null || bbox.minY == null || bbox.width == null || bbox.height == null)
			throw new JsonParseException("Unable to deserialize bounding box; need minX, minY, width, and height.",
					jp.getCurrentLocation());

		Rectangle2D ret = new Rectangle2D(bbox.minX, bbox.minY, bbox.width, bbox.height);
		return ret;
	}

}