package com.example.project;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Colors extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Rectangle rect = new Rectangle(300, 300);
		rect.setFill(Color.rgb(254, 235, 66, 0.3));

		Scene scene = new Scene(new BorderPane(rect));
		primaryStage.setScene(scene);

		ObservableList<Screen> screens = Screen.getScreens(); // Get list of Screens

		Rectangle2D bounds = screens.get(1).getVisualBounds();
		primaryStage.setX(bounds.getMinX());
		primaryStage.setY(bounds.getMinY());
		primaryStage.centerOnScreen();
//		primaryStage.setFullScreen(true);
		primaryStage.show();

	}

}
