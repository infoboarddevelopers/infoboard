package com.example.project;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.lestvitsa.InfoBoard.Application.Application;
import org.lestvitsa.InfoBoard.InfoModel.Utils.RestartApplicationUtil;
import org.lestvitsa.InfoBoard.LayoutBuilder.WheelMoveButton;
import org.lestvitsa.InfoBoard.LayoutBuilder.WheelMoveButton.Direction;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class Styles extends Application {

	private Button restart;

	@Override
	public void start(Stage primaryStage) {

		HBox hBox = new HBox();
		hBox.setSpacing(5);
		hBox.setTranslateY(30);

		restart = new Button("RESTART");
		restart.setLayoutX(10);
		restart.setLayoutY(10);

		restart.setOnAction(e -> {
			try {
				RestartApplicationUtil.restartApplication(null);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		});

		Button btn1 = new Button();
		btn1.getStyleClass().add("button1");
//		btn1.setPickOnBounds(true);
		Region icon = new Region();
		icon.getStyleClass().add("buttonIcon1");
		btn1.setGraphic(icon);


		Button btn2 = new Button("Button2");
		final ImageView imageView = new ImageView(
				new Image(getClass().getResource("../InfoModel/icons/icons8-undo-80.png").toExternalForm()));

		btn2.getStyleClass().add("button2");
		btn2.setGraphic(imageView);

		hBox.getChildren().addAll(restart, btn1, btn2);

		Pane root = new Pane(hBox);
		root.setId("MainScreen");

		createButtons(root);

		Scene scene = new Scene(root, 600, 600);
		scene.getStylesheets().add(getClass().getResource("test-styles.css").toExternalForm());
		setAccelerators(primaryStage, scene);

		primaryStage.setScene(scene);
		primaryStage.show();
	}

	private void setAccelerators(Stage primaryStage, Scene scene) {
		KeyCombination restartCombination = new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN,
				KeyCombination.ALT_DOWN);
		Runnable rn = () -> {
			restart.fire();
			System.out.println(restartCombination);
		};
		scene.getAccelerators().put(restartCombination, rn);

		KeyCombination reloadStyles = new KeyCodeCombination(KeyCode.F5);
		scene.getAccelerators().put(reloadStyles, () -> {
			List<String> stylesheets = scene.getStylesheets();
			List<String> clonedList = stylesheets.stream().collect(Collectors.toList());
			stylesheets.clear();
			stylesheets.addAll(clonedList);
			System.out.println(reloadStyles + " " + stylesheets);
		});
	}

	public void createButtons(Pane root) {

		WheelMoveButton moveButton = new WheelMoveButton(Direction.LEFT, 100);
		WheelMoveButton moveButton2 = new WheelMoveButton(Direction.RIGHT, 100);

		root.getChildren().addAll(moveButton, moveButton2);

		moveButton.setCenter(200, 200);
		moveButton2.setCenter(450, 200);

	}

	public static void main(String[] args) {
		launch(args);
	}

}
