package com.example.project;

import java.io.File;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

public class PlayVideo extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		// declare the media player and file to have class scope
		final File file = new File("/home/user/filesForInfoBoard/Администрация/Вальс-мюзет Свет и тени.mp4");
		final String MEDIA_URL = file.toURI().toString();

		// Create a Media
		final Media media = new Media(MEDIA_URL);

		// Create a Media Player
		final MediaPlayer player = new MediaPlayer(media);
		player.setAutoPlay(true);

		final MediaView mediaPane = new MediaView(player);

		BorderPane root = new BorderPane(mediaPane);
		Scene scene = new Scene(root, 800, 600);
		primaryStage.setScene(scene);
		primaryStage.show();

	}

}
