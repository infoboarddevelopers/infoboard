package org.lestvitsa.InfoBoard.Application;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class PdfViewerTest extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	public static BufferedImage toBufferedImage(java.awt.Image img) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}

		// Create a buffered image with transparency
		BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		// Draw the image on to the buffered image
		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();

		// Return the buffered image
		return bimage;
	}

	private static BufferedImage toImage(PDFPage page, int width, int height, boolean transparent) {
		int w = (int) page.getBBox().getWidth();
		int h = (int) page.getBBox().getHeight();
		java.awt.Rectangle rect = new java.awt.Rectangle(0, 0, w, h);

		BufferedImage bi = toBufferedImage(page.getImage(width, height, // width & height
				rect, // clip rect
				null, // null for the ImageObserver
				!transparent, // fill background with white
				true // block until drawing is done
		));

		return bi;
	}

	PDFFile loadTestPDF() {
		RandomAccessFile raf;
		try {
			java.net.URL url = this.getClass().getResource("../InfoModel/sample2.pdf");
			raf = new RandomAccessFile(new File(url.getPath()), "r");
			FileChannel fc = raf.getChannel();
			ByteBuffer buf = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
			return new PDFFile(buf);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	@Override
	public void start(Stage primaryStage) {
		try {
			PDFFile pdf = loadTestPDF();

			System.out.println(pdf);
			System.out.println(pdf.getNumPages());

			PDFPage firstPage = pdf.getPage(0);

			Rectangle2D box = firstPage.getPageBox();
			int width = (int) box.getWidth() * 4;
			int height = (int) box.getHeight() * 4;
			BufferedImage image = toImage(firstPage, width, height, true);
			System.out.println(image);

			Image fxImage = SwingFXUtils.toFXImage(image, null);
			ImageView sampleImageView = new ImageView(fxImage);
			sampleImageView.setFitWidth(width);
			sampleImageView.setFitHeight(height);
			sampleImageView.setPreserveRatio(false);
			sampleImageView.setSmooth(true); // the usage of the better filter

			BorderPane root = new BorderPane();
			root.setCenter(sampleImageView);
			Scene scene = new Scene(root, width, height);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
