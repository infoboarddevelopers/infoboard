package org.lestvitsa.InfoBoard.Application;

import org.lestvitsa.InfoBoard.InfoModel.Item;
import org.lestvitsa.InfoBoard.InfoModel.ListItem;
import org.lestvitsa.InfoBoard.InfoModel.PDFItem;
import org.lestvitsa.InfoBoard.InfoModel.VideoItem;
import org.lestvitsa.InfoBoard.LayoutBuilder.Builder;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TestMain2 extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		try {

//			Item item1 = ImageItem.createFromResource("TestImage.png");
//			Item item2 = PDFItem.createFromResource("sample1.pdf");
//			Item item3 = ImageItem.createFromResource("TestImage.png");
//			Item item4 = new WebItem("http://google.com", Item.getResourceURL("website.png").toString());
			Item item5 = PDFItem.createFromResource("sample3.pdf", false);
//			item5.setResourceIcon(("TestImage.png"));
//			Item[] arrayImageItems = ImageItem.createFromFolder("../");
//			ListItem listItem = new ListItem(ListItem.Mode.HORIZONTAL_CAROUSEL, arrayImageItems,
//					Item.getResourceURL("ball.jpg").toString());
//
//			Item item21 = ImageItem.createFromResource("TestImage.png");
//			Item item22 = ImageItem.createFromResource("TestImageLandscape.png");
//			Item item23 = new WebItem("http://lestvitsa.com/", Item.getResourceURL("lestvitsa.png").toString());
//			Item item24 = new WebItem("http://www.city.kharkov.ua/uk/", Item.getResourceURL("kharkov.png").toString());
//			Item item25 = PDFItem.createFromResource("sample2.pdf");
//
//			ListItem list0 = new ListItem(ListItem.Mode.PAGINATOR, new Item[] {listItem, item2, item4, item5 },
//					Item.getResourceURL("TestImageLandscape.png").toString());
//
//			ListItem list1 = new ListItem(ListItem.Mode.HORIZONTAL_LIST, new Item[] {
//					new BorderContainerItem(item21, new Rectangle2D(2, 2, 0, 0), new Rectangle2D(3, 4, 0, 0)),
//					new BorderContainerItem(item1, new Rectangle2D(2, 2, 0, 0), new Rectangle2D(3, 4, 0, 0)), item2,
//					// new BorderContainerItem(item2, new Rectangle2D(2, 2, 0, 0), new
//					// Rectangle2D(3, 4, 0, 0)),
//					list0, item4 });
//			list1.setResourceIcon("lestvitsa.png");
//
//			ListItem list2 = new ListItem(ListItem.Mode.HORIZONTAL_LIST, new Item[] { item22, item23, item24, item25 });
//			list2.setIcon(item25.getPreview());
//
//			ListItem list3 = new ListItem(ListItem.Mode.HORIZONTAL_CAROUSEL,
//					new Item[] { item1, item2, item3, item21, item22 });
//			list3.setResourceIcon("TestImageLandscape.png");
//
//			ListItem list4 = new ListItem(ListItem.Mode.HORIZONTAL_CAROUSEL,
//					ImageItem.createFromFolder("/home/grolm/Изображения/Фото1/"));
//			list4.setResourceIcon("TestImage.png");
//
//			ListItem list5 = new ListItem(ListItem.Mode.HORIZONTAL_CAROUSEL,
//					ImageItem.createFromFolder("/home/grolm/Изображения/Фото2/"));
//			list5.setResourceIcon("TestImage.png");
//
//			ListItem list6 = new ListItem(ListItem.Mode.VERTICAL_SIDEBAR, new Item[] { item2, item4, item5 },
//					Item.getResourceURL("TestImageLandscape.png").toString());
//
//			ListItem list7h = new ListItem(ListItem.Mode.HORIZONTAL_LIST, new Item[] { item5, item5, item5, item5 },
//					Item.getResourceURL("TestImageLandscape.png").toString());
//
//			ListItem list7 = new ListItem(ListItem.Mode.VERTICAL_LIST, new Item[] { list7h, list7h, list7h },
//					Item.getResourceURL("TestImageLandscape.png").toString());
			Item video = new VideoItem("video.mp4");
			Item[] items = { item5, item5, item5, item5, item5, video };
			ListItem list0 = new ListItem(ListItem.Mode.CIRCLES_FLOWER, items);
			list0.setResourceIcon("ball.jpg");
			ListItem list1 = new ListItem(ListItem.Mode.HORIZONTAL_CAROUSEL, items);
			ListItem list2 = new ListItem(ListItem.Mode.HORIZONTAL_LIST, items);
			ListItem list3 = new ListItem(ListItem.Mode.PAGINATOR, items);
			ListItem list4 = new ListItem(ListItem.Mode.VERTICAL_LIST, items);
			ListItem list5 = new ListItem(ListItem.Mode.VERTICAL_SIDEBAR, new Item[] { list1, list2, list3, list4 });

			ListItem list = new ListItem(ListItem.Mode.CIRCLES_FLOWER, new Item[] { list0, list1, list2, list3, list4,
					list5, video/* list6, list7 */ });

//			JSONObject jsonObject = new JSONObject(list);
//			System.out.println(jsonObject.toString(4));

			Builder b = new Builder();
			Parent root = b.createPresentation(list);
//			Parent root = b.createPresentation(new JSONLoader().loadJSONFromString(null));

			root.setId("MainScreen");

			Scene scene = new Scene(root, 800, 800);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setFullScreen(true);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
