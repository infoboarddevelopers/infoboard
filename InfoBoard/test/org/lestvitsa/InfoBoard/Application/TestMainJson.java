package org.lestvitsa.InfoBoard.Application;

import java.io.File;
import java.io.FileWriter;

import org.apache.commons.io.FileUtils;
import org.lestvitsa.InfoBoard.InfoModel.BorderContainerItem;
import org.lestvitsa.InfoBoard.InfoModel.FileSource;
import org.lestvitsa.InfoBoard.InfoModel.FileSource.Sort;
import org.lestvitsa.InfoBoard.InfoModel.FolderGridItem;
import org.lestvitsa.InfoBoard.InfoModel.FolderListItem;
import org.lestvitsa.InfoBoard.InfoModel.GridItem;
import org.lestvitsa.InfoBoard.InfoModel.ImageItem;
import org.lestvitsa.InfoBoard.InfoModel.Item;
import org.lestvitsa.InfoBoard.InfoModel.ListItem;
import org.lestvitsa.InfoBoard.InfoModel.PDFItem;
import org.lestvitsa.InfoBoard.InfoModel.VideoItem;
import org.lestvitsa.InfoBoard.InfoModel.WebItem;
import org.lestvitsa.InfoBoard.LayoutBuilder.Builder;
import org.tuiofx.TuioFX;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import javafx.geometry.Rectangle2D;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class TestMainJson extends Application {
	public static void main(String[] args) {
		TuioFX.enableJavaFXTouchProperties();
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		try {

			Item item1 = ImageItem.createFromResource("TestImage.png");
			Item item2 = PDFItem.createFromResource("sample1.pdf", false);
			item2.setName("PDF");
			Item item3 = ImageItem.createFromResource("TestImage.png");
			Item item4 = new WebItem("http://google.com", "website.png");
			item4.setName("Site viewer");
			Item item5 = PDFItem.createFromResource("sample3.pdf", false);
			item5.setName("Расписание занятий");
			item5.setResourceIcon(("TestImage.png"));
			ListItem listItem = new FolderListItem(ListItem.Mode.HORIZONTAL_CAROUSEL, "../", "ball.jpg", Sort.ByName,
					true);

			Item item21 = ImageItem.createFromResource("TestImage.png");
			Item item22 = ImageItem.createFromResource("TestImageLandscape.png");
			Item item23 = new WebItem("http://lestvitsa.com/", "lestvitsa.png");
			Item item24 = new WebItem("http://www.city.kharkov.ua/uk/", "kharkov.png");
			Item item25 = PDFItem.createFromResource("sample2.pdf", false);

			ListItem list0 = new ListItem(ListItem.Mode.PAGINATOR, new Item[] { listItem, item2, item4, item5 },
					"TestImageLandscape.png");

			ListItem list1 = new ListItem(ListItem.Mode.HORIZONTAL_LIST, new Item[] {
					new BorderContainerItem(item21, new Rectangle2D(2, 2, 0, 0), new Rectangle2D(3, 4, 0, 0)),
					new BorderContainerItem(item1, new Rectangle2D(2, 2, 0, 0), new Rectangle2D(3, 4, 0, 0)), item2,
					// new BorderContainerItem(item2, new Rectangle2D(2, 2, 0, 0), new
					// Rectangle2D(3, 4, 0, 0)),
					list0, item4 });
			list1.setResourceIcon("lestvitsa.png");

			ListItem list2 = new ListItem(ListItem.Mode.HORIZONTAL_LIST, new Item[] { item22, item23, item24, item25 });
			list2.setResourceIcon("sample2.pdf");

			ListItem list3 = new ListItem(ListItem.Mode.HORIZONTAL_CAROUSEL,
					new Item[] { item1, item2, item3, item21, item22 });
			list3.setResourceIcon("TestImageLandscape.png");

			ListItem list4 = new FolderListItem(ListItem.Mode.HORIZONTAL_CAROUSEL,
					FileSource.create(FileSource.Type.PresentationResource, "Фото Антон"), Sort.ByName, true);

			list4.setResourceIcon("TestImage.png");

			ListItem list5 = new FolderListItem(ListItem.Mode.HORIZONTAL_CAROUSEL,
					FileUtils.getUserDirectoryPath() + File.separator + "Presentation/Фото Антон/", Sort.ByName, true);

			list5.setResourceIcon("blue-sunny-sky.png");
			list5.setName("Фото Антон");

			ListItem list6 = new ListItem(ListItem.Mode.VERTICAL_SIDEBAR, new Item[] { item2, item4, item5 },
					"TestImageLandscape.png");

			GridItem list7h = new GridItem(480, 640,
					new Item[] { item1, item2, item3, item4, item5, item5, item5, item5, item5, item5 },
					"TestImageLandscape.png");

			ListItem list7 = new ListItem(ListItem.Mode.VERTICAL_LIST, new Item[] { list7h, list7h, list7h },
					"TestImageLandscape.png");

			Item videoItem1 = new VideoItem(FileSource.create(FileSource.Type.PresentationResource, "Landscape.mp4"));
			videoItem1.setName("Лагерь 2019");
			Item videoItem2 = new VideoItem(FileSource.create(FileSource.Type.PresentationResource, "Portrait.mp4"));
			videoItem2.setName("Лагерь 2019");
			Item videoItem3 = new VideoItem(FileSource.create(FileSource.Type.PresentationResource, "Landscape2.mp4"));
			Item videoItem4 = new VideoItem(FileSource.create(FileSource.Type.PresentationResource, "Portrait2.mp4"));
			videoItem4.setName("Параскева");
			Item videoItem5 = new VideoItem(FileSource.create(FileSource.Type.PresentationResource, "Landscape3.mp4"));
			videoItem5.setName("Лодка на озере");
			Item videoItem6 = new VideoItem(FileSource.create(FileSource.Type.PresentationResource, "Portrait3.mp4"));
			Item videoItem7 = new VideoItem(FileSource.create(FileSource.Type.PresentationResource, "output.mp4"));

			GridItem grid8 = new GridItem(800, 600,
					new Item[] { videoItem7, videoItem1, videoItem2, videoItem3, videoItem4, videoItem5, videoItem6 });
			grid8.setName("Видео");

			GridItem grid9 = new FolderGridItem(800, 600,
					FileSource.create(FileSource.Type.PresentationResource, "Mults"), Sort.ByName, true);
			grid9.setName("Наши любимые мультфильмы");

			GridItem grid10 = new FolderGridItem(800, 600,
					FileSource.create(FileSource.Type.PresentationResource, "PDFs"), Sort.ByName, true);

			ListItem list = new ListItem(ListItem.Mode.CIRCLES_FLOWER,
					new Item[] { list0, list1, list2, list3, list4, list5, list6, list7, grid8, grid9, grid10 });

			ObjectMapper mapper = new ObjectMapper();
			mapper.enableDefaultTyping();
			SimpleModule deser = new SimpleModule();
			deser.addDeserializer(Rectangle2D.class, new Rectangle2DDeserializer());
			mapper.registerModule(deser);

			String jsonDataString = mapper.writeValueAsString(list);

			FileWriter w = new FileWriter(getPresentationPath());
			w.write(jsonDataString);
			w.close();

			System.out.println(jsonDataString);

			list = mapper.readValue(jsonDataString, ListItem.class);

			Builder b = new Builder();
			Region root = b.createPresentation(list);

			runStageWithRoot(primaryStage, root);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
