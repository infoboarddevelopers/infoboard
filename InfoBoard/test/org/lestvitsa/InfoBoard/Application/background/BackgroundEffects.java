package org.lestvitsa.InfoBoard.Application.background;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.effect.Bloom;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.effect.Glow;
import javafx.scene.effect.Lighting;
import javafx.scene.effect.SepiaTone;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class BackgroundEffects extends Application {

	private Glow glow;
	private Bloom bloom;
	private GaussianBlur gaussianBlur;
	private Lighting lighting;
	private ColorAdjust colorAdjust;
	private SepiaTone sepiaTone;

	@Override
	public void start(Stage primaryStage) throws Exception {
		MediaView background = createBackground("background.mp4");

		StackPane root = new StackPane(new BorderPane(background));
		background.fitHeightProperty().bind(root.heightProperty());
//		background.fitWidthProperty().bind(root.widthProperty());

		Scene scene = new Scene(root, 1920, 1080);
		primaryStage.setScene(scene);

		ObservableList<Screen> screens = Screen.getScreens(); // Get list of Screens
		if (screens.size() > 1) {
			Screen screen2 = screens.get(1);
			Rectangle2D bounds = screen2.getVisualBounds();
			primaryStage.setX(bounds.getMinX());
			primaryStage.setY(bounds.getMinY());
		}

		primaryStage.setFullScreen(true);
		primaryStage.show();

		createControlsWindow();
	}

	private void createControlsWindow() {

		GridPane pane = new GridPane();
//		pane.setGridLinesVisible(true);
		pane.setPadding(new Insets(10));
		pane.setAlignment(Pos.CENTER);
		pane.setVgap(10);
		pane.setHgap(70);

		Label brightnessLabel = new Label("Яркость");
		Label contrastLabel = new Label("Контрастность");
		Label saturationLabel = new Label("Насыщенность");
		Label hueLabel = new Label("Оттенок");

		Label sepiaLabel = new Label("Сепия");

		Label glowLabel = new Label("Свечение");
		Label bloomLabel = new Label("Цветение");
		Label gaussianBlurLabel = new Label("Размытие");

		Label a2Label = new Label("Др.");
		Label a3Label = new Label("Др.");
		Label a4Label = new Label("Др.");
		Label a5Label = new Label("Др.");

		Slider brightnessSlider = new Slider(-1, 1, 0);
		Slider contrastSlider = new Slider(-1, 1, 0);
		Slider saturationSlider = new Slider(-1, 1, 0);
		Slider hueSlider = new Slider(-1, 1, 0);
		Slider sepiaSlider = new Slider(0, 1, 0);

		Slider glowSlider = new Slider(0, 1, 0);
		Slider bloomSlider = new Slider(0, 1, 0);
		Slider gaussianBlurSlider = new Slider(0, 63, 0);

		Slider a2Slider = new Slider(-1, 1, 0);
		Slider a3Slider = new Slider(-1, 1, 0);
		Slider a4Slider = new Slider(-1, 1, 0);
		Slider a5Slider = new Slider(-1, 1, 0);

		Label brightnessValue = new Label(String.format("%.2f", brightnessSlider.getValue()));
		Label contrastValue = new Label(String.format("%.2f", contrastSlider.getValue()));
		Label saturationValue = new Label(String.format("%.2f", saturationSlider.getValue()));
		Label hueValue = new Label(String.format("%.2f", hueSlider.getValue()));

		Label sepiaValue = new Label(String.format("%.2f", hueSlider.getValue()));

		Label glowValue = new Label(String.format("%.2f", hueSlider.getValue()));
		Label bloomValue = new Label(String.format("%.2f", hueSlider.getValue()));
		Label gaussianBlurValue = new Label(String.format("%.2f", hueSlider.getValue()));

		Label a2Value = new Label(String.format("%.2f", hueSlider.getValue()));
		Label a3Value = new Label(String.format("%.2f", hueSlider.getValue()));
		Label a4Value = new Label(String.format("%.2f", hueSlider.getValue()));
		Label a5Value = new Label(String.format("%.2f", hueSlider.getValue()));

		Button resetBtn = new Button("Сбросить");

		GridPane.setConstraints(brightnessLabel, 0, 0);
		GridPane.setConstraints(contrastLabel, 0, 1);
		GridPane.setConstraints(saturationLabel, 0, 2);
		GridPane.setConstraints(hueLabel, 0, 3);
		GridPane.setConstraints(sepiaLabel, 0, 4);
		GridPane.setConstraints(glowLabel, 0, 5);
		GridPane.setConstraints(bloomLabel, 0, 6);
		GridPane.setConstraints(gaussianBlurLabel, 0, 7);
		GridPane.setConstraints(a2Label, 0, 8);
		GridPane.setConstraints(a3Label, 0, 9);
		GridPane.setConstraints(a4Label, 0, 10);
		GridPane.setConstraints(a5Label, 0, 11);

		GridPane.setConstraints(brightnessSlider, 1, 0);
		GridPane.setConstraints(contrastSlider, 1, 1);
		GridPane.setConstraints(saturationSlider, 1, 2);
		GridPane.setConstraints(hueSlider, 1, 3);
		GridPane.setConstraints(sepiaSlider, 1, 4);
		GridPane.setConstraints(glowSlider, 1, 5);
		GridPane.setConstraints(bloomSlider, 1, 6);
		GridPane.setConstraints(gaussianBlurSlider, 1, 7);
		GridPane.setConstraints(a2Slider, 1, 8);
		GridPane.setConstraints(a3Slider, 1, 9);
		GridPane.setConstraints(a4Slider, 1, 10);
		GridPane.setConstraints(a5Slider, 1, 11);

		GridPane.setConstraints(brightnessValue, 2, 0);
		GridPane.setConstraints(contrastValue, 2, 1);
		GridPane.setConstraints(saturationValue, 2, 2);
		GridPane.setConstraints(hueValue, 2, 3);
		GridPane.setConstraints(sepiaValue, 2, 4);
		GridPane.setConstraints(glowValue, 2, 5);
		GridPane.setConstraints(bloomValue, 2, 6);
		GridPane.setConstraints(gaussianBlurValue, 2, 7);
		GridPane.setConstraints(a2Value, 2, 8);
		GridPane.setConstraints(a3Value, 2, 9);
		GridPane.setConstraints(a4Value, 2, 10);
		GridPane.setConstraints(a5Value, 2, 11);

		GridPane.setConstraints(resetBtn, 2, 12);

		brightnessSlider.setBlockIncrement(0.05);
		brightnessSlider.setPrefWidth(400);
		contrastSlider.setBlockIncrement(0.05);
		contrastSlider.setPrefWidth(400);
		saturationSlider.setBlockIncrement(0.05);
		saturationSlider.setPrefWidth(400);
		hueSlider.setBlockIncrement(0.05);
		hueSlider.setPrefWidth(400);

		sepiaSlider.setBlockIncrement(0.05);
		sepiaSlider.setPrefWidth(400);
		glowSlider.setBlockIncrement(0.05);
		glowSlider.setPrefWidth(400);
		bloomSlider.setBlockIncrement(0.05);
		bloomSlider.setPrefWidth(400);
		gaussianBlurSlider.setBlockIncrement(0.05);
		gaussianBlurSlider.setPrefWidth(400);
		a2Slider.setBlockIncrement(0.05);
		a2Slider.setPrefWidth(400);
		a3Slider.setBlockIncrement(0.05);
		a3Slider.setPrefWidth(400);
		a4Slider.setBlockIncrement(0.05);
		a4Slider.setPrefWidth(400);
		a5Slider.setBlockIncrement(0.05);
		a5Slider.setPrefWidth(400);

		brightnessSlider.valueProperty().addListener((ov, oldVal, newVal) -> {
			colorAdjust.setBrightness(newVal.doubleValue());
			brightnessValue.setText(String.format("%.2f", newVal));
		});
		contrastSlider.valueProperty().addListener((ov, oldVal, newVal) -> {
			colorAdjust.setContrast(newVal.doubleValue());
			contrastValue.setText(String.format("%.2f", newVal));
		});
		saturationSlider.valueProperty().addListener((ov, oldVal, newVal) -> {
			colorAdjust.setSaturation(newVal.doubleValue());
			saturationValue.setText(String.format("%.2f", newVal));
		});
		hueSlider.valueProperty().addListener((ov, oldVal, newVal) -> {
			colorAdjust.setHue(newVal.doubleValue());
			hueValue.setText(String.format("%.2f", newVal));
		});

		sepiaSlider.valueProperty().addListener((ov, oldVal, newVal) -> {
			sepiaTone.setLevel(newVal.doubleValue());
			sepiaValue.setText(String.format("%.2f", newVal));
		});

		glowSlider.valueProperty().addListener((ov, oldVal, newVal) -> {
			glow.setLevel(newVal.doubleValue());
			glowValue.setText(String.format("%.2f", newVal));
		});
		bloomSlider.valueProperty().addListener((ov, oldVal, newVal) -> {
			bloom.setThreshold(newVal.doubleValue());
			bloomValue.setText(String.format("%.2f", newVal));
		});
		gaussianBlurSlider.valueProperty().addListener((ov, oldVal, newVal) -> {
			gaussianBlur.setRadius(newVal.doubleValue());
			gaussianBlurValue.setText(String.format("%.2f", newVal));
		});

		resetBtn.setOnAction((v) -> {
			brightnessSlider.setValue(0);
			contrastSlider.setValue(0);
			saturationSlider.setValue(0);
			hueSlider.setValue(0);

			sepiaSlider.setValue(0);
			glowSlider.setValue(0);
			bloomSlider.setValue(0);
			gaussianBlurSlider.setValue(0);
		});

		pane.getChildren().addAll(brightnessLabel, brightnessSlider, brightnessValue);
		pane.getChildren().addAll(contrastLabel, contrastSlider, contrastValue);
		pane.getChildren().addAll(saturationLabel, saturationSlider, saturationValue);
		pane.getChildren().addAll(hueLabel, hueSlider, hueValue);

		pane.getChildren().addAll(sepiaLabel, sepiaSlider, sepiaValue);
		pane.getChildren().addAll(glowLabel, glowSlider, glowValue);
		pane.getChildren().addAll(bloomLabel, bloomSlider, bloomValue);
		pane.getChildren().addAll(gaussianBlurLabel, gaussianBlurSlider, gaussianBlurValue);

		pane.getChildren().addAll(a2Label, a2Slider, a2Value);
		pane.getChildren().addAll(a3Label, a3Slider, a3Value);
		pane.getChildren().addAll(a4Label, a4Slider, a4Value);
		pane.getChildren().addAll(a5Label, a5Slider, a5Value);

		pane.getChildren().add(resetBtn);

		Stage stage = new Stage();

//		Group rootGroup = new Group();
//		rootGroup.getChildren().add(text);

		Scene scene = new Scene(pane, 800, 350);

		stage.setScene(scene);
		stage.setTitle("Controls");
		stage.centerOnScreen();
		stage.show();
	}

	private MediaView createBackground(String path) {
		String url = org.lestvitsa.InfoBoard.Application.Application.class.getResource(path).toExternalForm();
		Media media = new Media(url);
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.setAutoPlay(true);
		mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
		MediaView mediaView = new MediaView(mediaPlayer);

		setEffects(mediaView);

		mediaView.setId("presentationsBackground");
		return mediaView;
	}

	private void setEffects(MediaView mediaView) {
		colorAdjust = new ColorAdjust();
		mediaView.setEffect(colorAdjust);

		sepiaTone = new SepiaTone();
		sepiaTone.setLevel(0);
		mediaView.setEffect(sepiaTone);

		glow = new Glow();
		glow.setLevel(0.7);
		mediaView.setEffect(glow);

		bloom = new Bloom();
		bloom.setThreshold(0);
		mediaView.setEffect(bloom);

		gaussianBlur = new GaussianBlur();
		gaussianBlur.setRadius(0);
		mediaView.setEffect(gaussianBlur);

//		lighting = new Lighting();
//		mediaView.setEffect(lighting);

	}

	public static void main(String[] args) {
		launch(args);
	}
}
