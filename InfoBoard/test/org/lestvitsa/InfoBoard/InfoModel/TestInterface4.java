package org.lestvitsa.InfoBoard.InfoModel;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.max;

import org.lestvitsa.InfoBoard.InfoModel.Utils.CoordinateMapper;
import org.lestvitsa.InfoBoard.InfoModel.Utils.Coordinates;

import javafx.beans.InvalidationListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class TestInterface4 extends javafx.application.Application {

	private double deltaAngle;

	private static final class Anchor {
		public double mouseAnchorX;
		public double mouseAnchorY;
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Pane root = new Pane();

		root.setId("MainScreen");

		setCirclesCarousel(root);

		Scene scene = new Scene(root, 800, 600);
		scene.getStylesheets().add(org.lestvitsa.InfoBoard.Application.Application.class.getResource("application.css").toExternalForm());

		primaryStage.setScene(scene);
//		primaryStage.setFullScreen(true);
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

	private void setCirclesCarousel(Pane root) {

		final int COUNT_OF_CIRCLES = 6;
		CirclesWheelTest circlesWheel = new CirclesWheelTest(COUNT_OF_CIRCLES);

		final Anchor anchorPoint = new Anchor();
		CoordinateMapper mapper = new CoordinateMapper();

		circlesWheel.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				anchorPoint.mouseAnchorX = event.getSceneX();
				anchorPoint.mouseAnchorY = event.getSceneY();
			}
		});

		final BooleanProperty isDragged = new SimpleBooleanProperty(false);

		circlesWheel.addEventFilter(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
			private double anchorA;

			@Override
			public void handle(MouseEvent event) {

				if (!isDragged.get()) {
					Point2D anchorP = mapper.mapToLocalCoordinates(anchorPoint.mouseAnchorX, anchorPoint.mouseAnchorY);
					anchorA = (Math.toDegrees(Coordinates.toPolarSystem(anchorP).angle) + 360) % 360;
				}

				Point2D curP = mapper.mapToLocalCoordinates(event.getSceneX(), event.getSceneY());
				double curA = (Math.toDegrees(Coordinates.toPolarSystem(curP).angle) + 360) % 360;

//				System.out.println("curA: " + curA + " anchorA: " + anchorA);

				deltaAngle = (abs(curA - anchorA) > 180)
						? (max(anchorA, curA) == curA) ? curA - 360 - anchorA : curA - (anchorA - 360)
						: curA - anchorA;

				// System.out.println("deltaAngle: " + deltaAngle);

				circlesWheel.turnWheel(-deltaAngle);

				anchorA = curA;

				isDragged.set(true);
			}
		});

		circlesWheel.addEventFilter(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (isDragged.get()) {
					circlesWheel.startRotationAnimation(abs(deltaAngle));

					isDragged.set(false);
					event.consume();
				}
			}

		});

		root.getChildren().add(circlesWheel);

		InvalidationListener positionUpdater = (l) -> {
			double rootWidth = root.getWidth();
			double rootHeight = root.getHeight();
			final double CENTER_X = rootWidth / 2;
			final double CENTER_Y = rootHeight * 0.1;
			final double WHEEL_RADIUS = (rootHeight - CENTER_Y) * 0.7;

			double perim = 2 * PI * WHEEL_RADIUS;
			// TODO
			double itemCircleRadius = perim / (COUNT_OF_CIRCLES + 6) / 2;

			circlesWheel.updatePositionAndSize(CENTER_X, CENTER_Y, WHEEL_RADIUS, itemCircleRadius);

			mapper.setGridCenter(new Point2D(CENTER_X, CENTER_Y));

//			System.out.println(
//					"New center [" + mapper.getGridCenter().getX() + ", " + mapper.getGridCenter().getY() + "]");

		};

		root.widthProperty().addListener(positionUpdater);
		root.heightProperty().addListener(positionUpdater);
		root.parentProperty().addListener(positionUpdater);

	}

}