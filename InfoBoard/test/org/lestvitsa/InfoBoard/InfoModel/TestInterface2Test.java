package org.lestvitsa.InfoBoard.InfoModel;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;

import org.junit.jupiter.api.Test;

class TestInterface2Test extends TestInterface2 {

	private int inquireRotateDirection(double prevAngle, double angle) {
		if (angle % 360 > prevAngle%360)
			return 1;
		else
			return -1;
	}

	@Test
	public void tDirection() {
		Random rand = new Random(47);
		for (int i = 0; i < 20; i++) {
			int angle = rand.nextInt(361);
			int prevAngle = rand.nextInt(361);
			System.out.printf("%d --> %d : %d\n", prevAngle, angle, inquireRotateDirection(prevAngle, angle));

		}
	}

}
