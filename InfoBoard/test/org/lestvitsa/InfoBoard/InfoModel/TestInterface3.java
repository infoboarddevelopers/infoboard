package org.lestvitsa.InfoBoard.InfoModel;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.max;

import org.lestvitsa.InfoBoard.InfoModel.Utils.CoordinateMapper;
import org.lestvitsa.InfoBoard.InfoModel.Utils.Coordinates;
import org.lestvitsa.InfoBoard.LayoutBuilder.Builder;
import org.lestvitsa.InfoBoard.LayoutBuilder.ItemCircle;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.InvalidationListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import javafx.util.Duration;

public class TestInterface3 extends Application {

	private double deltaAngle;

	protected static class CirclesWheel extends Group {
		public final int COUNT_OF_CIRCLES;

		private ItemCircle[] circles;
		private double wheelRadius;

		private Rotate rotate;
		private Scale scale;
		private Scale[] scaleTransforms;

		/**
		 * positive clockwise direction 1 - positive -1 - negative
		 */
		private IntegerProperty directionOfRotation = new SimpleIntegerProperty(1);

		private double previousAngle;
		private double angle;

		public CirclesWheel(int countOfCircles) {
			COUNT_OF_CIRCLES = countOfCircles;
			circles = new ItemCircle[COUNT_OF_CIRCLES];
			for (int i = 0; i < circles.length; i++) {
				// TODO
				if (i == 0)
					circles[i] = LestvitsaPresentationRoot.createLocketCircle();
				else
					circles[i] = LestvitsaPresentationRoot.createLocketCircle();
				circles[i].getStyleClass().add(Builder.DefaultStyleClass);

				this.getChildren().add(circles[i]);
			}

			rotate = new Rotate(0);
			this.getTransforms().add(rotate);

			scaleTransforms = new Scale[circles.length];
			for (int i = 0; i < scaleTransforms.length; i++) {
				scaleTransforms[i] = new Scale();
				circles[i].getTransforms().add(scaleTransforms[i]);
			}
//			getRotateProperty().addListener(e -> startScaleAnimation());

		}

		public void updatePositionAndSize(double centerX, double centerY, double wheelRadius, double itemCircleRadius) {
			this.wheelRadius = wheelRadius;

			CoordinateMapper mapper = new CoordinateMapper();
			mapper.setGridCenter(new Point2D(centerX, centerY));

			double angleOffset = 2 * PI / COUNT_OF_CIRCLES;
			for (int i = 0; i < COUNT_OF_CIRCLES; i++) {
				circles[i].setRadius(itemCircleRadius);
				Point2D center = mapper
						.mapToPaneCoordinates(Coordinates.toCartesianSystem(wheelRadius, i * angleOffset + PI / 2));
				circles[i].setCenterXY(center);
				scaleTransforms[i].setPivotX(center.getX());
				scaleTransforms[i].setPivotY(center.getY());
			}

			rotate.setPivotX(centerX);
			rotate.setPivotY(centerY);
//			scale.setPivotX(centerX);
//			scale.setPivotY(0);

		}

		public DoubleProperty getRotateProperty() {
			DoubleProperty rotProperty = rotate.angleProperty();

			rotProperty.addListener((angle, oldvalue, newvalue) -> {
				for (int i = 0; i < circles.length; i++)
					circles[i].setRotate(-newvalue.doubleValue());
			});

			return rotProperty;
		}

		/**
		 * positive clockwise direction
		 * 
		 * @param deltaAngle in degrees
		 */
		public void turnWheel(double deltaAngle) {
			angle += deltaAngle;

			directionOfRotation.set(angle > previousAngle ? 1 : -1);
//			System.out.println("direction: " + directionOfRotation.get());

			getRotateProperty().set(angle);

			previousAngle = angle;

		}

		private void fixateRotation() {
			angle %= 360;
			getRotateProperty().set(angle);
		}

		public void startScaleAnimation() {
			scale = new Scale();
			circles[0].getTransforms().add(scale);

			Timeline timeline = new Timeline();
			timeline.setCycleCount(2);
			timeline.setAutoReverse(true);

			KeyValue valX = new KeyValue(scale.xProperty(), 2);
			KeyValue valY = new KeyValue(scale.yProperty(), 2);
//			KeyValue kvArray[][] = new KeyValue[scaleTransforms.length][2];
//			for (int i = 0; i < kvArray.length; i++) {
//				kvArray[i][0] = new KeyValue(scaleTransforms[i].xProperty(),2);
//			}
			KeyFrame kf = new KeyFrame(Duration.millis(1000), valX, valY);

			KeyFrame kfArray[] = new KeyFrame[scaleTransforms.length];
			for (int i = 0; i < kfArray.length; i++) {
				KeyValue kvX = new KeyValue(scaleTransforms[i].xProperty(), 2);
				KeyValue kvY = new KeyValue(scaleTransforms[i].yProperty(), 2);
				kfArray[i] = new KeyFrame(Duration.millis(1000), kvX, kvY);
			}
//			timeline.getKeyFrames().add(kf);
			timeline.getKeyFrames().addAll(kfArray);

			timeline.play();
		}

		public void startRotationAnimation(double speedRatio) {
			fixateRotation();

			Timeline timeline = new Timeline();

			double deltaAngle = 360 / COUNT_OF_CIRCLES;
			double brakingDistanceInDegrees = directionOfRotation.get() == 1 ? angle + deltaAngle - (angle % deltaAngle)
					: angle < 0 ? angle - deltaAngle - (angle % deltaAngle) : angle - (angle % deltaAngle);

			System.out.println("angle from: " + getRotateProperty().get() + " to " + brakingDistanceInDegrees);

			KeyValue keyValue = new KeyValue(getRotateProperty(), brakingDistanceInDegrees, Interpolator.EASE_OUT);

			EventHandler onFinished = t -> angle = getRotateProperty().get() % 360;

			System.out.println("speedRatio: " + (700 / speedRatio));
			KeyFrame keyFrame = new KeyFrame(Duration.millis(7000), onFinished, keyValue);

			timeline.getKeyFrames().add(keyFrame);
			timeline.play();

		}

	}

	private static final class Anchor {
		public double mouseAnchorX;
		public double mouseAnchorY;
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Pane root = new Pane();

		root.setStyle("-fx-background-color: brown;");

		setCirclesCarousel(root, null, null);

		Scene scene = new Scene(root, 800, 600);
		scene.getStylesheets().add(
				org.lestvitsa.InfoBoard.Application.Application.class.getResource("application.css").toExternalForm());

		primaryStage.setScene(scene);
//		primaryStage.setFullScreen(true);
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

	private void setCirclesCarousel(Pane root, ListItem listItem, ItemActionHandler actionHandler) {

		final int COUNT_OF_CIRCLES = 6;
		CirclesWheel circlesWheel = new CirclesWheel(COUNT_OF_CIRCLES);

		final Anchor anchorPoint = new Anchor();
		CoordinateMapper mapper = new CoordinateMapper();

		circlesWheel.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
//				System.out.println("MOUSE_PRESSED " + event);
				anchorPoint.mouseAnchorX = event.getSceneX();
				anchorPoint.mouseAnchorY = event.getSceneY();
			}
		});

		final BooleanProperty isDragged = new SimpleBooleanProperty(false);

		circlesWheel.addEventFilter(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
			private double anchorA;

			@Override
			public void handle(MouseEvent event) {

				if (!isDragged.get()) {
					Point2D anchorP = mapper.mapToLocalCoordinates(anchorPoint.mouseAnchorX, anchorPoint.mouseAnchorY);
					anchorA = (Math.toDegrees(Coordinates.toPolarSystem(anchorP).angle) + 360) % 360;
				}

				Point2D curP = mapper.mapToLocalCoordinates(event.getSceneX(), event.getSceneY());
				double curA = (Math.toDegrees(Coordinates.toPolarSystem(curP).angle) + 360) % 360;

//				System.out.println("curA: " + curA + " anchorA: " + anchorA);

				deltaAngle = (abs(curA - anchorA) > 180)
						? (max(anchorA, curA) == curA) ? curA - 360 - anchorA : curA - (anchorA - 360)
						: curA - anchorA;

				System.out.println("deltaAngle: " + deltaAngle);

				circlesWheel.turnWheel(-deltaAngle);

				anchorA = curA;

				isDragged.set(true);
			}
		});

		circlesWheel.addEventFilter(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (isDragged.get()) {
					circlesWheel.startRotationAnimation(abs(deltaAngle));

					isDragged.set(false);
					event.consume();
				}
			}

		});

		root.getChildren().add(circlesWheel);

		InvalidationListener positionUpdater = (l) -> {
			double rootWidth = root.getWidth();
			double rootHeight = root.getHeight();
			final double CENTER_X = rootWidth / 2;
			final double CENTER_Y = rootHeight * 0.4;
			final double WHEEL_RADIUS = (rootHeight - CENTER_Y) * 0.7;

			double perim = 2 * PI * WHEEL_RADIUS;
			// TODO
			double itemCircleRadius = perim / (COUNT_OF_CIRCLES + 6) / 2;

			circlesWheel.updatePositionAndSize(CENTER_X, CENTER_Y, WHEEL_RADIUS, itemCircleRadius);

			mapper.setGridCenter(new Point2D(CENTER_X, CENTER_Y));

//			System.out.println(
//					"New center [" + mapper.getGridCenter().getX() + ", " + mapper.getGridCenter().getY() + "]");

		};

		root.widthProperty().addListener(positionUpdater);
		root.heightProperty().addListener(positionUpdater);
		root.parentProperty().addListener(positionUpdater);

	}

}