package org.lestvitsa.InfoBoard.InfoModel;

import static java.lang.Math.PI;

import org.lestvitsa.InfoBoard.InfoModel.Utils.CoordinateMapper;
import org.lestvitsa.InfoBoard.InfoModel.Utils.Coordinates;
import org.lestvitsa.InfoBoard.InfoModel.Utils.PolarPoint2D;
import org.lestvitsa.InfoBoard.LayoutBuilder.ItemCircle;
import org.lestvitsa.InfoBoard.LayoutBuilder.WheelMoveButton;
import org.lestvitsa.InfoBoard.LayoutBuilder.WheelMoveButton.Direction;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.InvalidationListener;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;

public class TestInterface extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		Button restart = new Button("RESTART");
		restart.setLayoutX(10);
		restart.setLayoutY(10);

		restart.setOnMouseReleased(e -> {
			primaryStage.close();
			try {
				this.start(primaryStage);
			} catch (Exception e1) {
				throw new RuntimeException(e1);
			}
		});

		Pane root = new Pane(restart);

//		createButtons(root);
		root.setStyle("-fx-background-color: brown;");

		setCirclesCarousel(root, null, null);

		Scene scene = new Scene(root, 600, 600);
		scene.getStylesheets().add(
				org.lestvitsa.InfoBoard.Application.Application.class.getResource("application.css").toExternalForm());

		primaryStage.setScene(scene);
		primaryStage.setFullScreen(true);
		primaryStage.show();
	}

	public void createButtons(Pane root) {

		WheelMoveButton moveButton = new WheelMoveButton(Direction.LEFT, 100);
		WheelMoveButton moveButton2 = new WheelMoveButton(Direction.RIGHT, 100);

		root.getChildren().addAll(moveButton, moveButton2);

		moveButton.setCenter(200, 200);
		moveButton2.setCenter(450, 200);

	}

	public static void main(String[] args) {
		launch(args);
	}

	private void setCirclesCarousel(Pane root, ListItem listItem, ItemActionHandler actionHandler) {
		Group groupOfCircles = new Group();

		Pane circlesPane = new Pane();

		root.getChildren().addAll(circlesPane, groupOfCircles);
		circlesPane.setStyle("-fx-background-color: darkgreen ;");

		final int COUNT_OF_CIRCLES = 6;

		ItemCircle[] circles = new ItemCircle[COUNT_OF_CIRCLES];
		for (int i = 0; i < circles.length; i++) {
			circles[i] = LestvitsaPresentationRoot.createLocketCircle();
			groupOfCircles.getChildren().add(circles[i]);
		}

		InvalidationListener positionUpdater = (l) -> {
			double rootWidth = root.getWidth();
			double rootHeight = root.getHeight();
			double clsPnLocateX = rootWidth / 2 - rootWidth / 4;
			double clsPnLocateY = rootHeight * 0.7 - rootWidth / 4;

			double clsPnWidth = rootWidth / 2;
			double clsPnHeight = rootWidth / 2;
			double clsPnCenterX = clsPnLocateX + clsPnWidth / 2;
			double clsPnCenterY = clsPnLocateY + clsPnHeight / 2;

			double carouselRadius = clsPnWidth * 0.4;

			circlesPane.setLayoutX(clsPnLocateX);
			circlesPane.setLayoutY(clsPnLocateY);
			circlesPane.setPrefSize(clsPnWidth, clsPnHeight);

			CoordinateMapper mapper = new CoordinateMapper(new Point2D(clsPnCenterX, clsPnCenterY));
			System.out.println("[" + mapper.getGridCenter().getX() + ", " + mapper.getGridCenter().getY() + "]");

			PolarPoint2D[] circlesCoordinates = new PolarPoint2D[COUNT_OF_CIRCLES];

			double angleOffset = 2 * PI / COUNT_OF_CIRCLES;
			for (int i = 0; i < COUNT_OF_CIRCLES; i++) {
				circlesCoordinates[i] = new PolarPoint2D(carouselRadius, i * angleOffset);
				circles[i].setRadius(clsPnHeight / 8);
				circles[i].setCenterXY(
						mapper.mapToPaneCoordinates(Coordinates.toCartesianSystem(circlesCoordinates[i])));
			}

			final Anchor anchorPoint = new Anchor();

			groupOfCircles.setOnMousePressed(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {

					anchorPoint.mouseAnchorX = event.getSceneX();
					anchorPoint.mouseAnchorY = event.getSceneY();

				}
			});

			groupOfCircles.setOnMouseDragged(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {

					Point2D curP = mapper.mapToLocalCoordinates(event.getSceneX(), event.getSceneY());
					Point2D anchorP = mapper.mapToLocalCoordinates(anchorPoint.mouseAnchorX, anchorPoint.mouseAnchorY);

					anchorPoint.angle = Coordinates.toPolarSystem(curP).angle
							- Coordinates.toPolarSystem(anchorP).angle;

					System.out.println("angle = " + anchorPoint.angle);

					for (int i = 0; i < circles.length; i++) {
						circles[i].setCenterXY(mapper.mapToPaneCoordinates(Coordinates.toCartesianSystem(
								carouselRadius, circlesCoordinates[i].angle + anchorPoint.angle)));

					}
				}
			});

			groupOfCircles.setOnMouseReleased(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {
					for (int i = 0; i < circles.length; i++) {
						circlesCoordinates[i].angle += anchorPoint.angle;
					}
					Timeline timeline = new Timeline();

					KeyValue keyValue = new KeyValue(groupOfCircles.rotateProperty(), groupOfCircles.getRotate() + 60,
							Interpolator.EASE_BOTH);
					KeyFrame keyFrame = new KeyFrame(Duration.millis(1000), keyValue);

					timeline.getKeyFrames().add(keyFrame);
					timeline.play();
				}

			});

		};

		root.widthProperty().addListener(positionUpdater);
		root.heightProperty().addListener(positionUpdater);
		root.parentProperty().addListener(positionUpdater);

	}

	private static final class Anchor {
		public double mouseAnchorX;
		public double mouseAnchorY;
		public double angle;
	}

}
