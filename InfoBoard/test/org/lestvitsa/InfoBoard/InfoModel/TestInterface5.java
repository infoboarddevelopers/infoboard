package org.lestvitsa.InfoBoard.InfoModel;

import java.util.Timer;

import org.lestvitsa.InfoBoard.LayoutBuilder.CirclesWheelPane;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class TestInterface5 extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		AnchorPane resultPane = new AnchorPane();
		ListItem listItem = new ListItem(ListItem.Mode.HORIZONTAL_LIST, new Item[20]);

		CirclesWheelPane circlesPane = new CirclesWheelPane(9, listItem, null);
		AnchorPane.setTopAnchor(circlesPane, 0.0);
		AnchorPane.setLeftAnchor(circlesPane, 0.0);
		AnchorPane.setBottomAnchor(circlesPane, 0.0);
		AnchorPane.setRightAnchor(circlesPane, 0.0);
		resultPane.getChildren().add(circlesPane);

		Rectangle2D screenBounds = Screen.getPrimary().getBounds();
		Scene scene = new Scene(resultPane, screenBounds.getWidth()/2, screenBounds.getHeight());
		scene.getStylesheets().add(
				org.lestvitsa.InfoBoard.Application.Application.class.getResource("application.css").toExternalForm());

		primaryStage.setScene(scene);
		primaryStage.setX(screenBounds.getWidth()/2);
		primaryStage.setY(0);
		primaryStage.show();

	}
	
	@Override
	public void stop() throws Exception {
		System.gc();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
