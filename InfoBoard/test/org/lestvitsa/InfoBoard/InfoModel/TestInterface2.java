package org.lestvitsa.InfoBoard.InfoModel;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.max;

import org.lestvitsa.InfoBoard.InfoModel.Utils.CoordinateMapper;
import org.lestvitsa.InfoBoard.InfoModel.Utils.Coordinates;
import org.lestvitsa.InfoBoard.LayoutBuilder.Builder;
import org.lestvitsa.InfoBoard.LayoutBuilder.ItemCircle;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.InvalidationListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

public class TestInterface2 extends Application {

	protected static class CirclesWheel2 extends Group {
		public final int COUNT_OF_CIRCLES;

		private ItemCircle[] circles;
		private CoordinateMapper mapper;
		private double wheelRadius;

		private Rotate rotate;

		private double anchorAngle;

		/**
		 * positive clockwise direction 1 - positive -1 - negative
		 */
		private IntegerProperty directionOfRotation = new SimpleIntegerProperty(1);

		private double previousAngle;

		public CirclesWheel2(int countOfCircles) {
			COUNT_OF_CIRCLES = countOfCircles;
			circles = new ItemCircle[COUNT_OF_CIRCLES];
			rotate = new Rotate(0);
			this.getTransforms().add(rotate);

			for (int i = 0; i < circles.length; i++) {
				// TODO
				if (i == 0)
					circles[i] = LestvitsaPresentationRoot.createLocketCircle();
				else
					circles[i] = LestvitsaPresentationRoot.createLocketCircle();
				circles[i].getStyleClass().add(Builder.DefaultStyleClass);

				this.getChildren().add(circles[i]);
			}

		}

		public void updatePositionAndSize(double centerX, double centerY, double wheelRadius, double itemCircleRadius) {
			this.wheelRadius = wheelRadius;

			mapper = new CoordinateMapper(new Point2D(centerX, centerY));

			double angleOffset = 2 * PI / COUNT_OF_CIRCLES;
			for (int i = 0; i < COUNT_OF_CIRCLES; i++) {
				circles[i].setRadius(itemCircleRadius);
				circles[i].setCenterXY(mapper.mapToPaneCoordinates(
						Coordinates.toCartesianSystem(wheelRadius, i * angleOffset + angleOffset / 2)));
			}

			rotate.setPivotX(centerX);
			rotate.setPivotY(centerY);

		}

		public DoubleProperty getRotateProperty() {
			DoubleProperty rotProperty = rotate.angleProperty();

			rotProperty.addListener((angle, oldvalue, newvalue) -> {
				for (int i = 0; i < circles.length; i++)
					circles[i].setRotate(-newvalue.doubleValue());
			});

			return rotProperty;
		}

		/**
		 * positive clockwise direction
		 * 
		 * @param angle in degrees
		 */
		public void rotate(double angle) {
			System.out.println("angle for rotate: " + angle);

			System.out.println("previousAngle: " + previousAngle);

			directionOfRotation.set(inquireRotateDirection(previousAngle +anchorAngle, angle+anchorAngle ));
			System.out.println("direction: " + directionOfRotation.get());
			
			getRotateProperty().set(angle + anchorAngle);

			previousAngle = angle;
		}

		private int inquireRotateDirection(double prevAngle, double angle) {
			if (abs(angle - prevAngle) > 180)
				if (max(prevAngle, angle) == angle)
					angle -= 360;
				else
					prevAngle -= 360;

			if (angle > prevAngle)
				return 1;
			else
				return -1;
		}

		public void fixateRotation() {
			anchorAngle = rotate.getAngle() % 360;
			System.out.println("anchorAngle: " + anchorAngle);
		}

		public void startScaleAnimation() {

		}

		public void startRotationAnimation() {

			Timeline timeline = new Timeline();

//			double brakingDistanceInDegrees = anchorAngle
//					- (directionOfRotation.get() > 0 ? 1 : -1) * anchorAngle % (360 / COUNT_OF_CIRCLES);

			double deltaAngle = 360 / COUNT_OF_CIRCLES;
			double brakingDistanceInDegrees = anchorAngle + directionOfRotation.get() * deltaAngle
					- Math.abs(anchorAngle % deltaAngle);

			System.out.println("brakingDistanceInDegrees: " + brakingDistanceInDegrees);

			KeyValue keyValue = new KeyValue(getRotateProperty(), brakingDistanceInDegrees, Interpolator.EASE_OUT);

			EventHandler onFinished = t -> fixateRotation();
			KeyFrame keyFrame = new KeyFrame(Duration.millis(700), onFinished, keyValue);

			timeline.getKeyFrames().add(keyFrame);
			timeline.play();

		}

	}

	private static final class Anchor {
		public double mouseAnchorX;
		public double mouseAnchorY;
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Pane root = new Pane();

		root.setStyle("-fx-background-color: brown;");

		setCirclesCarousel(root, null, null);

		Scene scene = new Scene(root, 800, 600);
		scene.getStylesheets().add(
				org.lestvitsa.InfoBoard.Application.Application.class.getResource("application.css").toExternalForm());

		primaryStage.setScene(scene);
//		primaryStage.setFullScreen(true);
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

	private void setCirclesCarousel(Pane root, ListItem listItem, ItemActionHandler actionHandler) {

		final int COUNT_OF_CIRCLES = 6;
		CirclesWheel2 circlesWheel = new CirclesWheel2(COUNT_OF_CIRCLES);
		root.getChildren().add(circlesWheel);

		InvalidationListener positionUpdater = (l) -> {
			double rootWidth = root.getWidth();
			double rootHeight = root.getHeight();
			final double CENTER_X = rootWidth / 2;
			final double CENTER_Y = rootHeight * 0.4;
			final double WHEEL_RADIUS = (rootHeight - CENTER_Y) * 0.7;

			double perim = 2 * PI * WHEEL_RADIUS;
			// TODO
			double itemCircleRadius = perim / (COUNT_OF_CIRCLES + 6) / 2;

			circlesWheel.updatePositionAndSize(CENTER_X, CENTER_Y, WHEEL_RADIUS, itemCircleRadius);

			CoordinateMapper mapper = new CoordinateMapper(new Point2D(CENTER_X, CENTER_Y));

			System.out.println("[" + mapper.getGridCenter().getX() + ", " + mapper.getGridCenter().getY() + "]");

			final Anchor anchorPoint = new Anchor();

			final BooleanProperty isDragged = new SimpleBooleanProperty(false);

			circlesWheel.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					anchorPoint.mouseAnchorX = event.getSceneX();
					anchorPoint.mouseAnchorY = event.getSceneY();
				}
			});

			circlesWheel.addEventFilter(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {

					Point2D curP = mapper.mapToLocalCoordinates(event.getSceneX(), event.getSceneY());
					Point2D anchorP = mapper.mapToLocalCoordinates(anchorPoint.mouseAnchorX, anchorPoint.mouseAnchorY);

					double angle = (Math.toDegrees(
							Coordinates.toPolarSystem(anchorP).angle - Coordinates.toPolarSystem(curP).angle) + 360)
							% 360;

					circlesWheel.rotate(angle);

					isDragged.set(true);
				}
			});

			circlesWheel.addEventFilter(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {
					if (isDragged.get()) {
						circlesWheel.fixateRotation();
						circlesWheel.startRotationAnimation();

						isDragged.set(false);
						event.consume();
					}
				}

			});

		};

		root.widthProperty().addListener(positionUpdater);
		root.heightProperty().addListener(positionUpdater);
		root.parentProperty().addListener(positionUpdater);

	}

}