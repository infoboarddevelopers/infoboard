package org.lestvitsa.InfoBoard.InfoModel;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class ItemTest extends Application {

	public static void main(String... args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Pane root = new Pane();
//		root.setId("mediaView");
//		Rectangle background = new Rectangle(600, 600);
//		background.getStyleClass().add("frontCircleBackground");
//		background.setFill(Color.RED);
//		root.setCenter(background);

		Scene scene = new Scene(root, 800, 600);
		scene.getStylesheets().add(
				org.lestvitsa.InfoBoard.Application.Application.class.getResource("application.css").toExternalForm());

		Image image = Item.createIconWithText(null, "lala"/* , root */);

		root.getChildren().add(new ImageView(image));

		primaryStage.setScene(scene);
		primaryStage.show();
	}

}
