#!/usr/bin/env python3.8
# -*- coding: UTF-8 -*-

import subprocess

def run_proc(cmd):
    return subprocess.run(cmd, shell=True, 
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT, 
        universal_newlines=True, check=True)

def get_input_device():
    cmd = '''
    xinput list | grep "Touch Controller" | grep id= | cut -f 2 | cut -d = -f 2 
    '''
    inputD_proc = run_proc(cmd)
    inputD = inputD_proc.stdout.split('\n')[0];
    return inputD

def get_display():
    cmd = '''
    xrandr | grep " connected" | grep VGA | cut -f 1 -d ' ' 
    '''
    display_proc = run_proc(cmd)
    display = display_proc.stdout
    return display

def send_events():
    cmd_lsinput = '''
    echo Zhenya+123 | sudo -S lsinput
    '''
    input_events = run_proc(cmd_lsinput)
    for device in input_events.stdout.split('/dev/input/event'):
        if device.find('Touch ') != -1:
            num = device.split('\n')[0]
            break
    mtdev_proc = subprocess.Popen([
        'sudo',
        '/home/user/DEVELOPMENT/mtdev2tuio-master/mtdev2tuio',
        '/dev/input/event{}'.format(num),
        'osc.udp://localhost:3333'
    ])
    print('/dev/input/event', num, sep='')
    return mtdev_proc


def main():
    # ограничение прередачи событий с сесора на конкретный экран
    cmd = 'xinput map-to-output {} {}'.format(get_input_device(), get_display())
    print(run_proc(cmd).stdout)

    # отправка событий на порт 3333 в фоне
    mtdev_proc = send_events()

    # запуск InfoBoard
    info_board = '/home/user/filesForInfoBoard/start_application.sh'
    subprocess.run(info_board, shell=True, universal_newlines=True, check=True)

    mtdev_proc.terminate()

main()


