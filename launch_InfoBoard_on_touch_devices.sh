#!/bin/bash

inputD=`xinput list | grep "Touch Device" | grep id= | cut -f 2 | cut -d = -f 2 | cut -d $'\n' -f 1`
display=`xrandr | grep " connected" | grep HDMI | cut -f 1 -d ' '`

xinput map-to-output $inputD $display

/home/user/filesForInfoBoard/start_application.sh
#/usr/lib/jvm/java-8-oracle/bin/java -jar '/home/user/filesForInfoBoard/InfoBoard.jar' --presentation=/home/user/filesForInfoBoard/main.json

